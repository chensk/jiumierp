/*
Navicat MySQL Data Transfer

Source Server         : 本地
Source Server Version : 80012
Source Host           : localhost:3306
Source Database       : jiumierp

Target Server Type    : MYSQL
Target Server Version : 80012
File Encoding         : 65001

Date: 2023-07-21 15:28:07
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `base_paper_size`
-- ----------------------------
DROP TABLE IF EXISTS `base_paper_size`;
CREATE TABLE `base_paper_size` (
  `size_code` varchar(20) DEFAULT NULL COMMENT '开数编号',
  `size_name` varchar(20) DEFAULT NULL COMMENT '名称',
  `size_type` varchar(1) DEFAULT NULL COMMENT '大度正度',
  `size_long` int(4) DEFAULT NULL,
  `size_width` int(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='纸张尺寸';

-- ----------------------------
-- Records of base_paper_size
-- ----------------------------

-- ----------------------------
-- Table structure for `base_security`
-- ----------------------------
DROP TABLE IF EXISTS `base_security`;
CREATE TABLE `base_security` (
  `code` varchar(200) DEFAULT NULL COMMENT '安全码',
  `key_val` varchar(200) DEFAULT NULL COMMENT 'KEY'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='安全码';

-- ----------------------------
-- Records of base_security
-- ----------------------------

-- ----------------------------
-- Table structure for `base_sequence`
-- ----------------------------
DROP TABLE IF EXISTS `base_sequence`;
CREATE TABLE `base_sequence` (
  `seq_name` varchar(12) NOT NULL COMMENT '序列号名称',
  `current_value` int(12) NOT NULL COMMENT '当前值',
  `increment` int(12) NOT NULL DEFAULT '1' COMMENT '增量'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 CHECKSUM=1 DELAY_KEY_WRITE=1 ROW_FORMAT=DYNAMIC COMMENT='序列表';

-- ----------------------------
-- Records of base_sequence
-- ----------------------------

-- ----------------------------
-- Table structure for `config_board`
-- ----------------------------
DROP TABLE IF EXISTS `config_board`;
CREATE TABLE `config_board` (
  `id` int(12) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(2000) DEFAULT '' COMMENT '备注',
  `status` varchar(20) DEFAULT 'vaild' COMMENT '状态',
  `board_name` varchar(50) NOT NULL COMMENT '板材名称',
  `warehouse_id` int(12) DEFAULT NULL COMMENT '仓库',
  `shelves_no` varchar(20) DEFAULT NULL COMMENT '货架号',
  `board_size` varchar(200) DEFAULT NULL COMMENT '刀模规格',
  `customer_id` int(12) DEFAULT NULL COMMENT '客户',
  `product_id` int(12) DEFAULT NULL COMMENT '产品',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='板材配置';

-- ----------------------------
-- Records of config_board
-- ----------------------------

-- ----------------------------
-- Table structure for `config_carton_shape`
-- ----------------------------
DROP TABLE IF EXISTS `config_carton_shape`;
CREATE TABLE `config_carton_shape` (
  `id` int(12) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(2000) DEFAULT '' COMMENT '备注',
  `status` varchar(20) DEFAULT 'vaild' COMMENT '状态',
  `carton_shape_name` varchar(50) NOT NULL COMMENT '箱型名称',
  `carton_shape_img` varchar(2000) DEFAULT NULL COMMENT '箱型图片',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COMMENT='箱型配置';

-- ----------------------------
-- Records of config_carton_shape
-- ----------------------------

-- ----------------------------
-- Table structure for `config_corrugated`
-- ----------------------------
DROP TABLE IF EXISTS `config_corrugated`;
CREATE TABLE `config_corrugated` (
  `id` int(12) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(2000) DEFAULT '' COMMENT '备注',
  `status` varchar(20) DEFAULT 'vaild' COMMENT '状态',
  `corrugated_name` varchar(50) NOT NULL COMMENT '楞型名称',
  `corrugated_rate` double(12,4) DEFAULT '0.0000' COMMENT '楞率',
  `corrugated_high` double(12,4) DEFAULT '0.0000' COMMENT '楞高',
  `board_level` int(8) DEFAULT '1' COMMENT '纸板层数',
  `price_coefficient` double(12,4) DEFAULT '0.0000' COMMENT '报价系数',
  `load_coefficient` double(12,4) DEFAULT '0.0000' COMMENT '装载系数',
  `freight_coefficient` double(12,4) DEFAULT '0.0000' COMMENT '运费系数',
  `corrugated_seq` varchar(20) DEFAULT NULL COMMENT '实际楞序',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='楞型配置';

-- ----------------------------
-- Records of config_corrugated
-- ----------------------------

-- ----------------------------
-- Table structure for `config_customer`
-- ----------------------------
DROP TABLE IF EXISTS `config_customer`;
CREATE TABLE `config_customer` (
  `id` int(12) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(2000) DEFAULT '' COMMENT '备注',
  `status` varchar(20) DEFAULT 'vaild' COMMENT '状态',
  `customer_name` varchar(50) NOT NULL COMMENT '客户名称',
  `customer_industry` varchar(20) DEFAULT NULL COMMENT '客户行业',
  `salesman_id` int(12) DEFAULT NULL COMMENT '销售员',
  `payment_type` varchar(20) DEFAULT NULL COMMENT '付款方式',
  `settlement_type` varchar(20) DEFAULT NULL COMMENT '结款方式',
  `delivery_type` varchar(20) DEFAULT NULL COMMENT '交货方式',
  `contact` varchar(50) DEFAULT NULL COMMENT '联系人',
  `tax_rate` double(12,4) DEFAULT '0.0000' COMMENT '税率',
  `address` varchar(500) DEFAULT NULL COMMENT '地址',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=196 DEFAULT CHARSET=utf8 COMMENT='客户信息';

-- ----------------------------
-- Records of config_customer
-- ----------------------------

-- ----------------------------
-- Table structure for `config_cutter_die`
-- ----------------------------
DROP TABLE IF EXISTS `config_cutter_die`;
CREATE TABLE `config_cutter_die` (
  `id` int(12) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(2000) DEFAULT '' COMMENT '备注',
  `status` varchar(20) DEFAULT 'vaild' COMMENT '状态',
  `cutter_die_name` varchar(50) NOT NULL COMMENT '刀模名称',
  `warehouse_id` int(12) DEFAULT NULL COMMENT '仓库',
  `shelves_no` varchar(20) DEFAULT NULL COMMENT '货架号',
  `cutter_die_size` varchar(200) DEFAULT NULL COMMENT '刀模规格',
  `customer_id` int(12) DEFAULT NULL COMMENT '客户',
  `product_id` int(12) DEFAULT NULL COMMENT '产品',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='刀模配置';

-- ----------------------------
-- Records of config_cutter_die
-- ----------------------------

-- ----------------------------
-- Table structure for `config_employee`
-- ----------------------------
DROP TABLE IF EXISTS `config_employee`;
CREATE TABLE `config_employee` (
  `id` int(12) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(2000) DEFAULT '' COMMENT '备注',
  `status` varchar(20) DEFAULT 'vaild' COMMENT '状态',
  `employee_name` varchar(50) NOT NULL COMMENT '姓名',
  `photo_img` text COMMENT '照片',
  `employee_no` varchar(50) DEFAULT NULL COMMENT '工号',
  `id_number` varchar(20) DEFAULT NULL COMMENT '身份证号',
  `pinyin` varchar(50) DEFAULT NULL COMMENT '拼音',
  `gender` varchar(1) DEFAULT '2' COMMENT '性别',
  `cell_phone` varchar(50) DEFAULT NULL COMMENT '手机号',
  `wechat` varchar(200) DEFAULT NULL COMMENT '微信号',
  `marital_status` varchar(20) DEFAULT NULL COMMENT '婚姻状况',
  `education_background` varchar(20) DEFAULT NULL COMMENT '学历',
  `dept_id` int(12) DEFAULT NULL COMMENT '部门',
  `job_post` varchar(20) DEFAULT NULL COMMENT '岗位',
  `job_position` varchar(20) DEFAULT NULL COMMENT '职位',
  `job_status` varchar(20) DEFAULT NULL COMMENT '在职状态',
  `wage` int(8) DEFAULT '0' COMMENT '基本工资',
  `bank_account` varchar(50) DEFAULT NULL COMMENT '银行账号',
  `is_labor_contract` varchar(1) DEFAULT 'Y' COMMENT '是否签订劳动合同',
  `is_social_security` varchar(1) DEFAULT 'Y' COMMENT '是否购买社保',
  `labor_contract_start_date` date DEFAULT NULL COMMENT '劳动合同签订日期',
  `labor_contract_end_date` date DEFAULT NULL COMMENT '劳动合同到期日期',
  `social_security_start_date` date DEFAULT NULL COMMENT '社保开始日期',
  `social_security_end_date` date DEFAULT NULL COMMENT '社保结束日期',
  `entry_date` date DEFAULT NULL COMMENT '入职日期',
  `dimission_date` date DEFAULT NULL COMMENT '离职日期',
  `dorm` varchar(50) DEFAULT NULL COMMENT '宿舍',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='员工信息';

-- ----------------------------
-- Records of config_employee
-- ----------------------------

-- ----------------------------
-- Table structure for `config_equipment`
-- ----------------------------
DROP TABLE IF EXISTS `config_equipment`;
CREATE TABLE `config_equipment` (
  `id` int(12) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(2000) DEFAULT '' COMMENT '备注',
  `status` varchar(20) DEFAULT 'vaild' COMMENT '状态',
  `equipment_name` varchar(50) NOT NULL COMMENT '设备名称',
  `equipment_model` varchar(50) DEFAULT NULL COMMENT '设备型号',
  `vendor` varchar(50) DEFAULT NULL COMMENT '生产厂商',
  `equipment_type` varchar(20) DEFAULT NULL COMMENT '设备分类',
  `equipment_purpose` varchar(20) DEFAULT NULL COMMENT '设备用途',
  `purchase_date` date DEFAULT NULL COMMENT '购买日期',
  `price` decimal(14,4) DEFAULT '0.0000' COMMENT '设备价格',
  `depreciation_type` varchar(20) DEFAULT NULL COMMENT '折旧方法',
  `depreciation_limit` int(8) DEFAULT '99' COMMENT '折旧年限',
  `dept_id` int(12) DEFAULT NULL COMMENT '使用部门',
  `batch_qty` int(8) DEFAULT '0' COMMENT '生产批量',
  `setup_hours` double(8,2) DEFAULT '0.00' COMMENT '生产准备时间(H)',
  `max_work_long` int(8) DEFAULT '0' COMMENT '最大上机规格长(mm)',
  `max_work_width` int(8) DEFAULT '0' COMMENT '最大上机规格宽(mm)',
  `min_work_long` int(8) DEFAULT '0' COMMENT '最小上机规格长(mm)',
  `min_work_width` int(8) DEFAULT '0' COMMENT '最小上机规格宽(mm)',
  `max_print_color` int(8) DEFAULT '1' COMMENT '最大印色',
  `production_report_type` varchar(20) DEFAULT NULL COMMENT '产量上报方式',
  `equipment_open` varchar(20) DEFAULT NULL COMMENT '设备开数',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8 COMMENT='设备管理';

-- ----------------------------
-- Records of config_equipment
-- ----------------------------

-- ----------------------------
-- Table structure for `config_formula`
-- ----------------------------
DROP TABLE IF EXISTS `config_formula`;
CREATE TABLE `config_formula` (
  `id` int(12) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(2000) DEFAULT '' COMMENT '备注',
  `status` varchar(20) DEFAULT 'vaild' COMMENT '状态',
  `formula_name` varchar(50) NOT NULL COMMENT '公式名称',
  `product_type` varchar(20) DEFAULT 'color' COMMENT '产品类型',
  `formula_desc` varchar(500) DEFAULT NULL COMMENT '公式说明',
  `formula_type` varchar(20) NOT NULL COMMENT '公式功能',
  `is_custom` varchar(1) DEFAULT 'N' COMMENT '是否定制',
  `formula_icon` varchar(50) DEFAULT 'fa fa-calculator' COMMENT '公式图标',
  `custom_formula_code` varchar(200) DEFAULT NULL COMMENT '定制公式编码',
  `extra_long` int(8) DEFAULT '0' COMMENT '长放数',
  `extra_width` int(8) DEFAULT '0' COMMENT '宽放数',
  `extra_height` int(8) DEFAULT '0' COMMENT '高放数',
  `extra_open_long` int(8) DEFAULT '0' COMMENT '展长放数',
  `extra_open_width` int(8) DEFAULT '0' COMMENT '展宽放数',
  `side_size` int(8) DEFAULT '0' COMMENT '边',
  `hanging_size` int(8) DEFAULT '0' COMMENT '挂头',
  `dispense_size` int(8) DEFAULT '0' COMMENT '免口',
  `stick_size` int(8) DEFAULT '0' COMMENT '粘口',
  `open_long_formula` varchar(500) DEFAULT NULL COMMENT '展长公式',
  `open_width_formula` varchar(500) DEFAULT NULL COMMENT '展宽公式',
  `area_formula` varchar(200) DEFAULT NULL COMMENT '面积公式',
  `base_times` double(12,2) DEFAULT '1.00' COMMENT '整体价格倍率',
  `base_price` decimal(14,4) DEFAULT '0.0000' COMMENT '基础单价0.001',
  `min_price` decimal(14,4) DEFAULT '0.0000' COMMENT '最低单价',
  `max_price` decimal(14,4) DEFAULT '0.0000' COMMENT '最高单价',
  `price_bottom` decimal(14,4) DEFAULT '0.0000' COMMENT '价格下限',
  `price_ceiling` decimal(14,4) DEFAULT '0.0000' COMMENT '价格上限',
  `min_unit_qty` int(8) DEFAULT '1' COMMENT '最小单位数量',
  `base_min_qty` int(8) DEFAULT '0' COMMENT '基数值',
  `coefficient_qty` double(12,4) DEFAULT '0.0000' COMMENT '系数',
  `discount_times` double(12,4) DEFAULT '1.0000' COMMENT '整体倍率',
  `discount_qty` int(8) DEFAULT '0' COMMENT '整体倍率基数',
  `extra_price` decimal(14,4) DEFAULT '0.0000' COMMENT '额外价格',
  `extra_times` double(12,2) DEFAULT '1.00' COMMENT '额外价格倍率',
  `base_rate` double(12,4) DEFAULT '1.0000' COMMENT '基数比例',
  `loss_rate` double(12,4) DEFAULT '0.0000' COMMENT '放数比例',
  `base_qty_arr` varchar(2000) DEFAULT NULL COMMENT '基数组',
  `loss_qty_arr` varchar(2000) DEFAULT NULL COMMENT '放数组',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=144 DEFAULT CHARSET=utf8 COMMENT='公式配置';

-- ----------------------------
-- Records of config_formula
-- ----------------------------

-- ----------------------------
-- Table structure for `config_materials`
-- ----------------------------
DROP TABLE IF EXISTS `config_materials`;
CREATE TABLE `config_materials` (
  `id` int(12) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(2000) DEFAULT '' COMMENT '备注',
  `status` varchar(20) DEFAULT 'vaild' COMMENT '状态',
  `materials_name` varchar(50) NOT NULL COMMENT '材料名称',
  `materials_type` varchar(20) DEFAULT NULL COMMENT '材料类型',
  `main_materials_type` varchar(20) DEFAULT NULL COMMENT '主材类型',
  `size_long` int(8) DEFAULT '0' COMMENT '材料规格长(mm)',
  `size_width` int(8) DEFAULT '0' COMMENT '材料规格宽(mm)',
  `width` int(8) DEFAULT '0' COMMENT '门幅(mm)',
  `is_area_calculate` varchar(1) DEFAULT 'N' COMMENT '按面积计算',
  `weight` int(8) DEFAULT '0' COMMENT '克重(g)',
  `density` double(12,4) DEFAULT '0.0000' COMMENT '密度(g/mm³)',
  `thickness` double(12,4) DEFAULT '0.0000' COMMENT '厚度(mm)',
  `corrugated_id` int(12) DEFAULT NULL COMMENT '楞型',
  `paper_formula_id` int(12) DEFAULT NULL COMMENT '纸张配方',
  `quality_days` int(8) DEFAULT '0' COMMENT '保质期(天)',
  `purchase_cycle` int(8) DEFAULT '0' COMMENT '采购周期(天)',
  `supplier_id` int(12) DEFAULT NULL COMMENT '主要供应商',
  `exemption` varchar(1) DEFAULT 'N' COMMENT '免检',
  `purchase_price` decimal(14,4) DEFAULT '0.0000' COMMENT '采购单价',
  `sale_price` decimal(14,4) DEFAULT '0.0000' COMMENT '销售单价',
  `purchase_unit` varchar(20) DEFAULT NULL COMMENT '采购单位',
  `production_unit` varchar(20) DEFAULT NULL COMMENT '生产单位',
  `sale_unit` varchar(20) DEFAULT NULL COMMENT '销售单位',
  `width_unit` varchar(20) DEFAULT NULL COMMENT '门幅单位',
  `inventory_unit` varchar(20) DEFAULT NULL COMMENT '库存单位',
  `requirements` varchar(500) DEFAULT NULL COMMENT '材料要求',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5217 DEFAULT CHARSET=utf8 COMMENT='材料配置';

-- ----------------------------
-- Records of config_materials
-- ----------------------------

-- ----------------------------
-- Table structure for `config_paper_formula`
-- ----------------------------
DROP TABLE IF EXISTS `config_paper_formula`;
CREATE TABLE `config_paper_formula` (
  `id` int(12) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(2000) DEFAULT '' COMMENT '备注',
  `status` varchar(20) DEFAULT 'vaild' COMMENT '状态',
  `paper_formula_name` varchar(50) NOT NULL COMMENT '配方名称',
  `customer_material` varchar(20) DEFAULT NULL COMMENT '客户材质',
  `corrugated_id` int(12) DEFAULT NULL COMMENT '楞型',
  `surface_paper` varchar(20) DEFAULT NULL COMMENT '面纸',
  `bottom_paper` varchar(20) DEFAULT NULL COMMENT '底纸',
  `core_paper` varchar(2000) DEFAULT NULL COMMENT '芯纸',
  `corrugated_paper` varchar(2000) DEFAULT NULL COMMENT '楞纸',
  `org_paper_price` decimal(14,4) DEFAULT '0.0000' COMMENT '原纸成本',
  `carton_price` decimal(14,4) DEFAULT '0.0000' COMMENT '纸箱单价',
  `cardboard_price` decimal(14,4) DEFAULT '0.0000' COMMENT '纸板单价',
  `square_weight` int(8) DEFAULT '0' COMMENT '平方重量(g)',
  `edge_strength` varchar(50) DEFAULT NULL COMMENT '边压强度',
  `burst_strength` varchar(50) DEFAULT NULL COMMENT '耐破强度',
  `water_rate` double(8,2) DEFAULT '0.00' COMMENT '含水率(%)',
  `stiffness` varchar(50) DEFAULT NULL COMMENT '挺度',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='纸张配方';

-- ----------------------------
-- Records of config_paper_formula
-- ----------------------------

-- ----------------------------
-- Table structure for `config_permission`
-- ----------------------------
DROP TABLE IF EXISTS `config_permission`;
CREATE TABLE `config_permission` (
  `id` int(12) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(2000) DEFAULT '' COMMENT '备注',
  `status` varchar(20) DEFAULT 'vaild' COMMENT '状态',
  `user_id` int(12) NOT NULL COMMENT '用户',
  `customer_id` int(12) DEFAULT NULL COMMENT '客户',
  `supplier_id` int(12) DEFAULT NULL COMMENT '供应商',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='数据权限';

-- ----------------------------
-- Records of config_permission
-- ----------------------------

-- ----------------------------
-- Table structure for `config_process`
-- ----------------------------
DROP TABLE IF EXISTS `config_process`;
CREATE TABLE `config_process` (
  `id` int(12) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(2000) DEFAULT '' COMMENT '备注',
  `status` varchar(20) DEFAULT 'vaild' COMMENT '状态',
  `process_name` varchar(50) NOT NULL COMMENT '产品名称',
  `process_type` varchar(20) DEFAULT NULL COMMENT '产品类型',
  `equipment_id` int(12) DEFAULT NULL COMMENT '设备',
  `min_batch_qty` int(8) DEFAULT '1' COMMENT '生产最小批量',
  `semi_finished_unit` varchar(20) DEFAULT NULL COMMENT '半成品单位',
  `processing_type` varchar(20) DEFAULT NULL COMMENT '生产加工方式',
  `requirements` varchar(500) DEFAULT NULL COMMENT '工序要求',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=83 DEFAULT CHARSET=utf8 COMMENT='工序配置';

-- ----------------------------
-- Records of config_process
-- ----------------------------

-- ----------------------------
-- Table structure for `config_product`
-- ----------------------------
DROP TABLE IF EXISTS `config_product`;
CREATE TABLE `config_product` (
  `id` int(12) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(2000) DEFAULT '' COMMENT '备注',
  `status` varchar(20) DEFAULT 'vaild' COMMENT '状态',
  `product_name` varchar(50) NOT NULL COMMENT '产品名称',
  `product_type` varchar(20) NOT NULL DEFAULT 'color' COMMENT '彩印/纸箱',
  `customer_id` int(12) DEFAULT NULL COMMENT '客户',
  `is_public` varchar(1) DEFAULT 'Y' COMMENT '是否公共产品',
  `customer_material_no` varchar(50) DEFAULT NULL COMMENT '客户料号',
  `price` decimal(14,4) DEFAULT '0.0000' COMMENT '产品单价',
  `outsource_price` decimal(14,4) DEFAULT '0.0000' COMMENT '外发单价',
  `sale_unit` varchar(20) DEFAULT 'ones' COMMENT '销售单位',
  `production_unit` varchar(20) DEFAULT 'ones' COMMENT '生产单位',
  `sale_valuation_type` varchar(20) DEFAULT NULL COMMENT '计价方式(销售)',
  `outsource_valuation_type` varchar(20) DEFAULT NULL COMMENT '计价方式(外发)',
  `size_long` int(8) DEFAULT '0' COMMENT '产品规格(长)',
  `size_width` int(8) DEFAULT '0' COMMENT '产品规格(宽)',
  `size_height` int(8) DEFAULT '0' COMMENT '产品规格(高)',
  `opensize_long` int(8) DEFAULT '0' COMMENT '展长',
  `opensize_width` int(8) DEFAULT '0' COMMENT '展宽',
  `area` double(12,6) DEFAULT '0.000000' COMMENT '单只面积(m²)',
  `handwork_price` decimal(14,4) DEFAULT '0.0000' COMMENT '手工费',
  `formula_id` int(12) DEFAULT NULL COMMENT '面积公式',
  `product_img` text COMMENT '产品图片',
  `materials_id` int(12) DEFAULT NULL COMMENT '印刷主材',
  `quotation_template_id` int(12) DEFAULT NULL COMMENT '报价工艺卡',
  `production_template_id` int(12) DEFAULT NULL COMMENT '生产工艺卡',
  `work_long` int(8) DEFAULT '0' COMMENT '上机长(mm)',
  `work_width` int(8) DEFAULT '0' COMMENT '上机宽(mm)',
  `is_big_panel` varchar(1) DEFAULT 'N' COMMENT '大色面版',
  `is_special_paper` varchar(1) DEFAULT 'N' COMMENT '特殊纸张',
  `print_sheet_qty` int(8) DEFAULT '1' COMMENT '印张正数',
  `print_sheet_sum_qty` int(8) DEFAULT '1' COMMENT '总印张数',
  `puzzle_qty` int(8) DEFAULT '1' COMMENT '拼版数',
  `forme_qty` int(8) DEFAULT '1' COMMENT '印版付数',
  `print_type` varchar(20) DEFAULT NULL COMMENT '印刷方式',
  `gauge_type` varchar(20) DEFAULT NULL COMMENT '拉规方式',
  `is_buttom` varchar(1) DEFAULT 'Y' COMMENT '是否到底',
  `front_spot` varchar(500) DEFAULT NULL COMMENT '正面专色',
  `back_spot` varchar(500) DEFAULT NULL COMMENT '反面专色',
  `front_color` varchar(500) DEFAULT NULL COMMENT '正面颜色',
  `back_color` varchar(500) DEFAULT NULL COMMENT '反面颜色',
  `carton_shape_id` int(12) DEFAULT NULL COMMENT '箱型',
  `corrugated_id` int(12) DEFAULT NULL COMMENT '楞型',
  `paper_formula_id` int(12) DEFAULT NULL COMMENT '纸张配方',
  `carton_size_type` varchar(20) DEFAULT NULL COMMENT '生产类型',
  `hole_pattern` varchar(20) DEFAULT NULL COMMENT '孔型',
  `wire_type` varchar(20) DEFAULT NULL COMMENT '压线类型',
  `wire_deep` varchar(20) DEFAULT NULL COMMENT '压线深度',
  `cross_wise` varchar(500) DEFAULT NULL COMMENT '生产横向压线',
  `vertical_wise` varchar(500) DEFAULT NULL COMMENT '生产纵向压线',
  `production_size` varchar(500) DEFAULT NULL COMMENT '生产尺寸',
  `error_range` varchar(500) DEFAULT NULL COMMENT '误差范围',
  `print_technology_type` varchar(20) DEFAULT NULL COMMENT '印刷工艺',
  `print_times` int(8) DEFAULT '1' COMMENT '印刷次数',
  `plate_qty` int(8) DEFAULT '1' COMMENT '印版数',
  `die_warehouse_id` int(12) DEFAULT NULL COMMENT '原模仓库',
  `die_shelves_no` varchar(20) DEFAULT NULL COMMENT '原模货架号',
  `print_direction` varchar(20) DEFAULT NULL COMMENT '印刷方向',
  `is_aquatone` varchar(1) DEFAULT 'N' COMMENT '是否套色',
  `parent_id` int(12) DEFAULT NULL COMMENT '����ƷID',
  `product_level` varchar(20) DEFAULT 'N' COMMENT '��Ʒ�㼶',
  `product_parent_name` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5567 DEFAULT CHARSET=utf8 COMMENT='产品配置';

-- ----------------------------
-- Records of config_product
-- ----------------------------

-- ----------------------------
-- Table structure for `config_production_template`
-- ----------------------------
DROP TABLE IF EXISTS `config_production_template`;
CREATE TABLE `config_production_template` (
  `id` int(12) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(2000) DEFAULT '' COMMENT '备注',
  `status` varchar(20) DEFAULT 'vaild' COMMENT '状态',
  `template_name` varchar(200) NOT NULL COMMENT '工艺卡名称',
  `requirements` varchar(500) DEFAULT NULL COMMENT '工艺卡要求',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='生产工艺卡';

-- ----------------------------
-- Records of config_production_template
-- ----------------------------

-- ----------------------------
-- Table structure for `config_production_template_materials`
-- ----------------------------
DROP TABLE IF EXISTS `config_production_template_materials`;
CREATE TABLE `config_production_template_materials` (
  `id` int(12) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `remark` varchar(2000) DEFAULT '' COMMENT '备注',
  `production_template_id` int(12) NOT NULL COMMENT '报价工艺卡',
  `materials_id` int(12) NOT NULL COMMENT '材料',
  `process_id` int(12) DEFAULT NULL COMMENT '关联工序',
  `cutter_die_id` int(12) DEFAULT NULL COMMENT '刀模',
  `board_id` int(12) DEFAULT NULL COMMENT '板材',
  `is_get_process_qty` varchar(1) DEFAULT 'N' COMMENT '取工序数量',
  `base_rate` double(12,4) DEFAULT '1.0000' COMMENT '比例',
  `loss_rate` double(12,4) DEFAULT '0.0000' COMMENT '损耗率(%)',
  `loss_qty` int(8) DEFAULT '0' COMMENT '固定损耗数',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='生产工艺卡材料子表';

-- ----------------------------
-- Records of config_production_template_materials
-- ----------------------------

-- ----------------------------
-- Table structure for `config_production_template_process`
-- ----------------------------
DROP TABLE IF EXISTS `config_production_template_process`;
CREATE TABLE `config_production_template_process` (
  `id` int(12) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `remark` varchar(2000) DEFAULT '' COMMENT '备注',
  `production_template_id` int(12) NOT NULL COMMENT '报价工艺卡',
  `process_id` int(12) DEFAULT NULL COMMENT '工序',
  `times` double(12,4) DEFAULT '1.0000' COMMENT '倍率',
  `valuation_type` varchar(20) DEFAULT NULL COMMENT '计价方式',
  `is_outsource` varchar(1) DEFAULT 'N' COMMENT '外发',
  `is_with_materials` varchar(1) DEFAULT 'N' COMMENT '外发带料',
  `outsource_price` decimal(14,4) DEFAULT '0.0000' COMMENT '外发价格',
  `price` decimal(14,4) DEFAULT '0.0000' COMMENT '单价',
  `equipment_id` int(12) DEFAULT NULL COMMENT '设备',
  `loss_rate` double(12,4) DEFAULT '0.0000' COMMENT '损耗率(%)',
  `loss_qty` int(8) DEFAULT '0' COMMENT '固定损耗数',
  `process_order` int(4) DEFAULT '1' COMMENT '顺序',
  `is_time_count` varchar(1) DEFAULT NULL COMMENT '是否倍率计算',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='生产工艺卡工序子表';

-- ----------------------------
-- Records of config_production_template_process
-- ----------------------------

-- ----------------------------
-- Table structure for `config_quotation_template`
-- ----------------------------
DROP TABLE IF EXISTS `config_quotation_template`;
CREATE TABLE `config_quotation_template` (
  `id` int(12) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(2000) DEFAULT '' COMMENT '备注',
  `status` varchar(20) DEFAULT 'vaild' COMMENT '状态',
  `template_name` varchar(200) NOT NULL COMMENT '工艺卡名称',
  `requirements` varchar(500) DEFAULT NULL COMMENT '工艺卡要求',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='报价工艺卡';

-- ----------------------------
-- Records of config_quotation_template
-- ----------------------------

-- ----------------------------
-- Table structure for `config_quotation_template_materials`
-- ----------------------------
DROP TABLE IF EXISTS `config_quotation_template_materials`;
CREATE TABLE `config_quotation_template_materials` (
  `id` int(12) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `remark` varchar(2000) DEFAULT '' COMMENT '备注',
  `quotation_template_id` int(12) NOT NULL COMMENT '报价工艺卡',
  `materials_id` int(12) DEFAULT NULL COMMENT '材料',
  `process_id` int(12) DEFAULT NULL COMMENT '关联工序',
  `formula_id` int(12) DEFAULT NULL COMMENT '计算公式',
  `times` double(12,4) DEFAULT '1.0000' COMMENT '倍率',
  `price` decimal(14,4) DEFAULT '0.0000' COMMENT '单价',
  `is_get_process_qty` varchar(1) DEFAULT 'N' COMMENT '取工序数量',
  `loss_rate` double(12,4) DEFAULT '0.0000' COMMENT '损耗率(%)',
  `loss_qty` int(8) DEFAULT '0' COMMENT '固定损耗数',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='报价工艺卡材料子表';

-- ----------------------------
-- Records of config_quotation_template_materials
-- ----------------------------

-- ----------------------------
-- Table structure for `config_quotation_template_process`
-- ----------------------------
DROP TABLE IF EXISTS `config_quotation_template_process`;
CREATE TABLE `config_quotation_template_process` (
  `id` int(12) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `remark` varchar(2000) DEFAULT '' COMMENT '备注',
  `quotation_template_id` int(12) NOT NULL COMMENT '报价工艺卡',
  `process_id` int(12) DEFAULT NULL COMMENT '工序',
  `formula_id` int(12) DEFAULT NULL COMMENT '计算公式',
  `times` double(12,4) DEFAULT '1.0000' COMMENT '倍率',
  `price` decimal(14,4) DEFAULT '0.0000' COMMENT '单价',
  `equipment_id` int(12) DEFAULT NULL COMMENT '设备',
  `loss_rate` double(12,4) DEFAULT '0.0000' COMMENT '损耗率(%)',
  `loss_qty` int(8) DEFAULT '0' COMMENT '固定损耗数',
  `process_order` int(4) DEFAULT '1' COMMENT '顺序',
  `is_time_count` varchar(1) DEFAULT NULL COMMENT '是否倍率计算',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='报价工艺卡工序子表';

-- ----------------------------
-- Records of config_quotation_template_process
-- ----------------------------

-- ----------------------------
-- Table structure for `config_supplier`
-- ----------------------------
DROP TABLE IF EXISTS `config_supplier`;
CREATE TABLE `config_supplier` (
  `id` int(12) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(2000) DEFAULT '' COMMENT '备注',
  `status` varchar(20) DEFAULT 'vaild' COMMENT '状态',
  `supplier_name` varchar(50) NOT NULL COMMENT '供应商名称',
  `supplier_type` varchar(20) DEFAULT NULL COMMENT '供应商类型',
  `tax_rate` double(12,4) DEFAULT '0.0000' COMMENT '税率',
  `buyer_id` int(12) DEFAULT NULL COMMENT '采购员',
  `payment_type` varchar(20) DEFAULT NULL COMMENT '付款方式',
  `settlement_type` varchar(20) DEFAULT NULL COMMENT '结款方式',
  `delivery_type` varchar(20) DEFAULT NULL COMMENT '交货方式',
  `address` varchar(500) DEFAULT NULL COMMENT '地址',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=44 DEFAULT CHARSET=utf8 COMMENT='供应商信息';

-- ----------------------------
-- Records of config_supplier
-- ----------------------------

-- ----------------------------
-- Table structure for `config_supplier_contact`
-- ----------------------------
DROP TABLE IF EXISTS `config_supplier_contact`;
CREATE TABLE `config_supplier_contact` (
  `id` int(12) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `remark` varchar(2000) DEFAULT '' COMMENT '备注',
  `status` varchar(20) DEFAULT 'vaild' COMMENT '状态',
  `supplier_id` int(12) DEFAULT NULL COMMENT '供应商',
  `is_default` varchar(1) DEFAULT 'N' COMMENT '默认联系人',
  `contact_name` varchar(50) DEFAULT NULL COMMENT '联系人名称',
  `cell_phone` varchar(50) DEFAULT NULL COMMENT '手机号',
  `email` varchar(50) DEFAULT NULL COMMENT '邮箱',
  `qq_no` varchar(50) DEFAULT NULL COMMENT 'QQ号',
  `wechat` varchar(50) DEFAULT NULL COMMENT '微信',
  `invoice_unit` varchar(50) DEFAULT NULL COMMENT '开票单位',
  `address` varchar(500) DEFAULT NULL COMMENT '地址',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='供应商联系人';

-- ----------------------------
-- Records of config_supplier_contact
-- ----------------------------

-- ----------------------------
-- Table structure for `config_tax`
-- ----------------------------
DROP TABLE IF EXISTS `config_tax`;
CREATE TABLE `config_tax` (
  `id` int(12) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(2000) DEFAULT '' COMMENT '备注',
  `status` varchar(20) DEFAULT 'vaild' COMMENT '状态',
  `tax_name` varchar(50) NOT NULL COMMENT '名称',
  `tax_rate` double(12,4) NOT NULL DEFAULT '0.0000' COMMENT '税率',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COMMENT='税率表';

-- ----------------------------
-- Records of config_tax
-- ----------------------------

-- ----------------------------
-- Table structure for `config_team`
-- ----------------------------
DROP TABLE IF EXISTS `config_team`;
CREATE TABLE `config_team` (
  `id` int(12) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(2000) DEFAULT '' COMMENT '备注',
  `status` varchar(20) DEFAULT 'vaild' COMMENT '状态',
  `team_name` varchar(50) NOT NULL COMMENT '班组名称',
  `equipment_id` int(12) DEFAULT NULL COMMENT '设备',
  `standard_output` int(8) DEFAULT '0' COMMENT '标准产量',
  `less_price` decimal(14,4) DEFAULT '0.0000' COMMENT '未脱产单价',
  `more_price` decimal(14,4) DEFAULT '0.0000' COMMENT '超产单价',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COMMENT='班组配置';

-- ----------------------------
-- Records of config_team
-- ----------------------------

-- ----------------------------
-- Table structure for `config_team_member`
-- ----------------------------
DROP TABLE IF EXISTS `config_team_member`;
CREATE TABLE `config_team_member` (
  `id` int(12) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `remark` varchar(2000) DEFAULT '' COMMENT '备注',
  `team_id` int(12) DEFAULT NULL COMMENT '班组',
  `employee_id` int(12) DEFAULT NULL COMMENT '组员',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='班组组员';

-- ----------------------------
-- Records of config_team_member
-- ----------------------------

-- ----------------------------
-- Table structure for `config_warehouse`
-- ----------------------------
DROP TABLE IF EXISTS `config_warehouse`;
CREATE TABLE `config_warehouse` (
  `id` int(12) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(2000) DEFAULT '' COMMENT '备注',
  `status` varchar(20) DEFAULT 'vaild' COMMENT '状态',
  `warehouse_name` varchar(50) NOT NULL COMMENT '仓库名称',
  `warehouse_type` varchar(20) DEFAULT NULL COMMENT '仓库类型',
  `warehouse_address` varchar(200) DEFAULT NULL COMMENT '地址',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8 COMMENT='仓库配置';

-- ----------------------------
-- Records of config_warehouse
-- ----------------------------

-- ----------------------------
-- Table structure for `finance_daily`
-- ----------------------------
DROP TABLE IF EXISTS `finance_daily`;
CREATE TABLE `finance_daily` (
  `id` int(12) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(2000) DEFAULT '' COMMENT '备注',
  `status` varchar(20) DEFAULT 'draft' COMMENT '状态',
  `approver` varchar(50) DEFAULT NULL COMMENT '审批人',
  `approval_time` datetime DEFAULT NULL COMMENT '审批时间',
  `approval_type` varchar(20) DEFAULT 'N' COMMENT '审批类型',
  `serial_number` varchar(20) NOT NULL COMMENT '单号',
  `daily_date` date DEFAULT NULL COMMENT '日期',
  `amount` decimal(14,4) DEFAULT '0.0000' COMMENT '总金额',
  `attachment` text COMMENT '文件',
  `file_path` text COMMENT '文件路径',
  `file_name` varchar(200) DEFAULT NULL COMMENT '文件名称',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='生产日报';

-- ----------------------------
-- Records of finance_daily
-- ----------------------------

-- ----------------------------
-- Table structure for `finance_daily_detail`
-- ----------------------------
DROP TABLE IF EXISTS `finance_daily_detail`;
CREATE TABLE `finance_daily_detail` (
  `id` int(12) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `remark` varchar(2000) DEFAULT '' COMMENT '备注',
  `finance_daily_id` int(12) DEFAULT NULL COMMENT '财务日报',
  `produce_order_id` int(12) DEFAULT NULL COMMENT '施工单',
  `produce_schedule_id` int(12) DEFAULT NULL COMMENT '生产排程',
  `team_id` int(12) DEFAULT NULL COMMENT '班组',
  `finance_standard_id` int(12) DEFAULT NULL COMMENT '计费标准',
  `process_id` int(12) DEFAULT NULL COMMENT '工序',
  `product_id` int(12) DEFAULT NULL COMMENT '产品',
  `job_content` varchar(200) DEFAULT NULL COMMENT '工作内容',
  `allocation_type` varchar(20) DEFAULT NULL COMMENT '分配方式',
  `valuation_type` varchar(20) DEFAULT NULL COMMENT '计价类型',
  `start_time` datetime DEFAULT NULL COMMENT '开始时间',
  `end_time` datetime DEFAULT NULL COMMENT '结束时间',
  `work_time` double(12,2) DEFAULT NULL COMMENT '工作时间(H)',
  `qty` int(8) DEFAULT '0' COMMENT '数量',
  `price` decimal(14,4) DEFAULT '0.0000' COMMENT '单价',
  `amount` decimal(14,4) DEFAULT '0.0000' COMMENT '总金额',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='排程明细';

-- ----------------------------
-- Records of finance_daily_detail
-- ----------------------------

-- ----------------------------
-- Table structure for `finance_daily_detail_employee`
-- ----------------------------
DROP TABLE IF EXISTS `finance_daily_detail_employee`;
CREATE TABLE `finance_daily_detail_employee` (
  `id` int(12) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `remark` varchar(2000) DEFAULT '' COMMENT '备注',
  `finance_daily_id` int(12) DEFAULT NULL COMMENT '财务日报',
  `finance_daily_detail_id` int(12) DEFAULT NULL COMMENT '排程明细',
  `employee_id` int(12) DEFAULT NULL COMMENT '组员',
  `price` decimal(14,4) DEFAULT '0.0000' COMMENT '单价',
  `employee_price` decimal(14,4) DEFAULT '0.0000' COMMENT '员工单价',
  `employee_scale` double(12,2) DEFAULT '0.00' COMMENT '员工比例(%)',
  `process_order` int(4) DEFAULT NULL COMMENT '顺序',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='排程员工';

-- ----------------------------
-- Records of finance_daily_detail_employee
-- ----------------------------

-- ----------------------------
-- Table structure for `finance_standard`
-- ----------------------------
DROP TABLE IF EXISTS `finance_standard`;
CREATE TABLE `finance_standard` (
  `id` int(12) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(2000) DEFAULT '' COMMENT '备注',
  `status` varchar(20) DEFAULT 'vaild' COMMENT '状态',
  `product_id` int(12) DEFAULT NULL COMMENT '产品',
  `process_id` int(12) DEFAULT NULL COMMENT '工序',
  `job_content` varchar(200) DEFAULT NULL COMMENT '工作内容',
  `valuation_type` varchar(20) DEFAULT NULL COMMENT '计价类型',
  `calculate_type` varchar(20) DEFAULT NULL COMMENT '计算方式',
  `allocation_type` varchar(20) DEFAULT NULL COMMENT '分配方式',
  `equal_qty` int(8) DEFAULT '0' COMMENT '定量',
  `convert_qty` int(8) DEFAULT '0' COMMENT '折算定量',
  `equal_price` decimal(14,4) DEFAULT '0.0000' COMMENT '定量单价',
  `above_price` decimal(14,4) DEFAULT '0.0000' COMMENT '超产单价',
  `below_price` decimal(14,4) DEFAULT '0.0000' COMMENT '未超产单价',
  `scale_team` varchar(500) DEFAULT '0' COMMENT '分配比例(%)',
  `price_team` varchar(500) DEFAULT '0' COMMENT '分配单价',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='计费标准';

-- ----------------------------
-- Records of finance_standard
-- ----------------------------

-- ----------------------------
-- Table structure for `gen_table`
-- ----------------------------
DROP TABLE IF EXISTS `gen_table`;
CREATE TABLE `gen_table` (
  `table_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `table_name` varchar(200) DEFAULT '' COMMENT '表名称',
  `table_comment` varchar(500) DEFAULT '' COMMENT '表描述',
  `sub_table_name` varchar(64) DEFAULT NULL COMMENT '关联子表的表名',
  `sub_table_fk_name` varchar(64) DEFAULT NULL COMMENT '子表关联的外键名',
  `class_name` varchar(100) DEFAULT '' COMMENT '实体类名称',
  `tpl_category` varchar(200) DEFAULT 'crud' COMMENT '使用的模板（crud单表操作 tree树表操作 sub主子表操作）',
  `package_name` varchar(100) DEFAULT NULL COMMENT '生成包路径',
  `module_name` varchar(50) DEFAULT NULL COMMENT '生成模块名',
  `business_name` varchar(50) DEFAULT NULL COMMENT '生成业务名',
  `function_name` varchar(50) DEFAULT NULL COMMENT '生成功能名',
  `function_author` varchar(50) DEFAULT NULL COMMENT '生成功能作者',
  `gen_type` char(1) DEFAULT '0' COMMENT '生成代码方式（0zip压缩包 1自定义路径）',
  `gen_path` varchar(200) DEFAULT '/' COMMENT '生成路径（不填默认项目路径）',
  `options` varchar(1000) DEFAULT NULL COMMENT '其它生成选项',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`table_id`)
) ENGINE=InnoDB AUTO_INCREMENT=87 DEFAULT CHARSET=utf8 COMMENT='代码生成业务表';

-- ----------------------------
-- Records of gen_table
-- ----------------------------

-- ----------------------------
-- Table structure for `gen_table_column`
-- ----------------------------
DROP TABLE IF EXISTS `gen_table_column`;
CREATE TABLE `gen_table_column` (
  `column_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `table_id` varchar(64) DEFAULT NULL COMMENT '归属表编号',
  `column_name` varchar(200) DEFAULT NULL COMMENT '列名称',
  `column_comment` varchar(500) DEFAULT NULL COMMENT '列描述',
  `column_type` varchar(100) DEFAULT NULL COMMENT '列类型',
  `java_type` varchar(500) DEFAULT NULL COMMENT 'JAVA类型',
  `java_field` varchar(200) DEFAULT NULL COMMENT 'JAVA字段名',
  `is_pk` char(1) DEFAULT NULL COMMENT '是否主键（1是）',
  `is_increment` char(1) DEFAULT NULL COMMENT '是否自增（1是）',
  `is_required` char(1) DEFAULT NULL COMMENT '是否必填（1是）',
  `is_insert` char(1) DEFAULT NULL COMMENT '是否为插入字段（1是）',
  `is_edit` char(1) DEFAULT NULL COMMENT '是否编辑字段（1是）',
  `is_list` char(1) DEFAULT NULL COMMENT '是否列表字段（1是）',
  `is_query` char(1) DEFAULT NULL COMMENT '是否查询字段（1是）',
  `query_type` varchar(200) DEFAULT 'EQ' COMMENT '查询方式（等于、不等于、大于、小于、范围）',
  `html_type` varchar(200) DEFAULT NULL COMMENT '显示类型（文本框、文本域、下拉框、复选框、单选框、日期控件）',
  `dict_type` varchar(200) DEFAULT '' COMMENT '字典类型',
  `sort` int(11) DEFAULT NULL COMMENT '排序',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`column_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1604 DEFAULT CHARSET=utf8 COMMENT='代码生成业务表字段';

-- ----------------------------
-- Records of gen_table_column
-- ----------------------------

-- ----------------------------
-- Table structure for `outsource_checking`
-- ----------------------------
DROP TABLE IF EXISTS `outsource_checking`;
CREATE TABLE `outsource_checking` (
  `id` int(12) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(2000) DEFAULT '' COMMENT '备注',
  `status` varchar(20) DEFAULT 'draft' COMMENT '状态',
  `approver` varchar(50) DEFAULT NULL COMMENT '审批人',
  `approval_time` datetime DEFAULT NULL COMMENT '审批时间',
  `approval_type` varchar(20) DEFAULT 'N' COMMENT '审批类型',
  `serial_number` varchar(20) NOT NULL COMMENT '单号',
  `supplier_id` int(12) DEFAULT NULL COMMENT '供应商',
  `checking_date` date DEFAULT NULL COMMENT '对账日期',
  `payment_type` varchar(20) DEFAULT NULL COMMENT '付款方式',
  `settlement_type` varchar(20) DEFAULT NULL COMMENT '结款方式',
  `tax_rate` double(12,4) DEFAULT '0.0000' COMMENT '税率',
  `amount` decimal(14,4) DEFAULT '0.0000' COMMENT '总金额',
  `delivery_amount` decimal(14,4) DEFAULT '0.0000' COMMENT '到货总金额',
  `return_amount` decimal(14,4) DEFAULT '0.0000' COMMENT '退货总金额',
  `invoice_no` varchar(200) DEFAULT NULL COMMENT '发票号',
  `invoice_unit` varchar(200) DEFAULT NULL COMMENT '开票单位',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='外发对账';

-- ----------------------------
-- Records of outsource_checking
-- ----------------------------

-- ----------------------------
-- Table structure for `outsource_checking_process`
-- ----------------------------
DROP TABLE IF EXISTS `outsource_checking_process`;
CREATE TABLE `outsource_checking_process` (
  `id` int(12) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `remark` varchar(2000) DEFAULT '' COMMENT '备注',
  `outsource_checking_id` int(12) DEFAULT NULL COMMENT '外发对账单',
  `outsource_delivery_id` int(12) DEFAULT NULL COMMENT '外发到货',
  `outsource_delivery_process_id` int(12) DEFAULT NULL COMMENT '外发到货工序',
  `outsource_order_process_id` int(12) DEFAULT NULL COMMENT '外发加工工序',
  `outsource_order_id` int(12) DEFAULT NULL COMMENT '外发加工',
  `sale_order_product_id` int(12) DEFAULT NULL COMMENT '销售产品',
  `sale_order_id` int(12) DEFAULT NULL COMMENT '销售订单',
  `produce_order_id` int(12) DEFAULT NULL COMMENT '施工单',
  `produce_order_product_id` int(12) DEFAULT NULL COMMENT '施工产品',
  `product_id` int(12) DEFAULT NULL COMMENT '产品',
  `process_id` int(12) DEFAULT NULL COMMENT '工序',
  `production_template_id` int(12) DEFAULT NULL COMMENT '生产工艺卡',
  `production_template_process_id` int(12) DEFAULT NULL COMMENT '生产工艺卡工序',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='外发对账材料';

-- ----------------------------
-- Records of outsource_checking_process
-- ----------------------------

-- ----------------------------
-- Table structure for `outsource_delivery`
-- ----------------------------
DROP TABLE IF EXISTS `outsource_delivery`;
CREATE TABLE `outsource_delivery` (
  `id` int(12) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(2000) DEFAULT '' COMMENT '备注',
  `status` varchar(20) DEFAULT 'draft' COMMENT '状态',
  `approver` varchar(50) DEFAULT NULL COMMENT '审批人',
  `approval_time` datetime DEFAULT NULL COMMENT '审批时间',
  `approval_type` varchar(20) DEFAULT 'N' COMMENT '审批类型',
  `serial_number` varchar(20) NOT NULL COMMENT '单号',
  `supplier_id` int(12) DEFAULT NULL COMMENT '供应商',
  `contact` varchar(50) DEFAULT NULL COMMENT '联系人',
  `cell_phone` varchar(50) DEFAULT NULL COMMENT '手机号',
  `tax_rate` double(12,4) DEFAULT '0.0000' COMMENT '税率',
  `amount` decimal(14,4) DEFAULT '0.0000' COMMENT '总金额',
  `delivery_date` date DEFAULT NULL COMMENT '到货日期',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='外发到货';

-- ----------------------------
-- Records of outsource_delivery
-- ----------------------------

-- ----------------------------
-- Table structure for `outsource_delivery_process`
-- ----------------------------
DROP TABLE IF EXISTS `outsource_delivery_process`;
CREATE TABLE `outsource_delivery_process` (
  `id` int(12) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `remark` varchar(2000) DEFAULT '' COMMENT '备注',
  `outsource_delivery_id` int(12) DEFAULT NULL COMMENT '外发到货',
  `outsource_order_process_id` int(12) DEFAULT NULL COMMENT '外发加工工序',
  `outsource_order_id` int(12) NOT NULL COMMENT '外发加工',
  `sale_order_product_id` int(12) DEFAULT NULL COMMENT '销售产品',
  `sale_order_id` int(12) DEFAULT NULL COMMENT '销售订单',
  `produce_order_id` int(12) DEFAULT NULL COMMENT '施工单',
  `produce_order_process_id` int(12) DEFAULT NULL COMMENT '施工工序',
  `product_id` int(12) DEFAULT NULL COMMENT '产品',
  `process_id` int(12) DEFAULT NULL COMMENT '工序',
  `production_template_id` int(12) DEFAULT NULL COMMENT '生产工艺卡',
  `production_template_process_id` int(12) DEFAULT NULL COMMENT '生产工艺卡工序',
  `outsource_type` varchar(20) DEFAULT NULL COMMENT '外发类型',
  `outsource_valuation_type` varchar(20) DEFAULT NULL COMMENT '计价方式',
  `qty` int(8) DEFAULT '0' COMMENT '数量',
  `return_qty` int(8) DEFAULT '0' COMMENT '退货数量',
  `price` decimal(14,4) DEFAULT '0.0000' COMMENT '单价',
  `supplier_id` int(12) DEFAULT NULL COMMENT '供应商',
  `amount` decimal(14,4) DEFAULT '0.0000' COMMENT '总金额',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='外发到货工序';

-- ----------------------------
-- Records of outsource_delivery_process
-- ----------------------------

-- ----------------------------
-- Table structure for `outsource_order`
-- ----------------------------
DROP TABLE IF EXISTS `outsource_order`;
CREATE TABLE `outsource_order` (
  `id` int(12) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(2000) DEFAULT '' COMMENT '备注',
  `status` varchar(20) DEFAULT 'draft' COMMENT '状态',
  `approver` varchar(50) DEFAULT NULL COMMENT '审批人',
  `approval_time` datetime DEFAULT NULL COMMENT '审批时间',
  `approval_type` varchar(20) DEFAULT 'N' COMMENT '审批类型',
  `serial_number` varchar(20) NOT NULL COMMENT '单号',
  `supplier_id` int(12) DEFAULT NULL COMMENT '供应商',
  `contact` varchar(50) DEFAULT NULL COMMENT '联系人',
  `cell_phone` varchar(50) DEFAULT NULL COMMENT '手机号',
  `tax_rate` double(12,4) DEFAULT '0.0000' COMMENT '税率',
  `amount` decimal(14,4) DEFAULT '0.0000' COMMENT '总金额',
  `delivery_date` date DEFAULT NULL COMMENT '到货日期',
  `outsource_type` varchar(20) DEFAULT NULL COMMENT '外发类型',
  `finished_qty` int(12) DEFAULT NULL COMMENT '��Ʒ����',
  `customer_name` varchar(200) DEFAULT NULL COMMENT '�ͻ�',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COMMENT='外发加工';

-- ----------------------------
-- Records of outsource_order
-- ----------------------------

-- ----------------------------
-- Table structure for `outsource_order_materials`
-- ----------------------------
DROP TABLE IF EXISTS `outsource_order_materials`;
CREATE TABLE `outsource_order_materials` (
  `id` int(12) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `remark` varchar(2000) DEFAULT '' COMMENT '备注',
  `outsource_order_id` int(12) NOT NULL COMMENT '外发加工',
  `materials_id` int(12) DEFAULT NULL COMMENT '材料',
  `qty` int(8) DEFAULT '0' COMMENT '数量',
  `return_qty` int(8) DEFAULT '0' COMMENT '退回数量',
  `price` decimal(14,4) DEFAULT '0.0000' COMMENT '单价',
  `size_long` int(8) DEFAULT '0' COMMENT '规格长',
  `size_width` int(8) DEFAULT '0' COMMENT '规格宽',
  `warehouse_id` int(12) DEFAULT NULL COMMENT '仓库',
  `delivery_date` date DEFAULT NULL COMMENT '带料日期',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='外发加工带料';

-- ----------------------------
-- Records of outsource_order_materials
-- ----------------------------

-- ----------------------------
-- Table structure for `outsource_order_process`
-- ----------------------------
DROP TABLE IF EXISTS `outsource_order_process`;
CREATE TABLE `outsource_order_process` (
  `id` int(12) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `remark` varchar(2000) DEFAULT '' COMMENT '备注',
  `outsource_order_id` int(12) NOT NULL COMMENT '外发加工',
  `sale_order_product_id` int(12) DEFAULT NULL COMMENT '销售产品',
  `sale_order_id` int(12) DEFAULT NULL COMMENT '销售订单',
  `produce_order_id` int(12) DEFAULT NULL COMMENT '施工单',
  `produce_order_product_id` int(12) DEFAULT NULL COMMENT '施工产品',
  `produce_order_process_id` int(12) DEFAULT NULL COMMENT '施工工序',
  `product_id` int(12) DEFAULT NULL COMMENT '产品',
  `process_id` int(12) DEFAULT NULL COMMENT '工序',
  `supplier_id` int(12) DEFAULT NULL COMMENT '供应商',
  `production_template_id` int(12) DEFAULT NULL COMMENT '生产工艺卡',
  `production_template_process_id` int(12) DEFAULT NULL COMMENT '生产工艺卡工序',
  `outsource_type` varchar(20) DEFAULT NULL COMMENT '外发类型',
  `outsource_valuation_type` varchar(20) DEFAULT NULL COMMENT '计价方式',
  `qty` int(8) DEFAULT '0' COMMENT '数量',
  `price` decimal(14,4) DEFAULT '0.0000' COMMENT '单价',
  `amount` decimal(14,4) DEFAULT '0.0000' COMMENT '总金额',
  `materials_name` varchar(50) DEFAULT NULL COMMENT '��������',
  `materials_size` varchar(100) DEFAULT NULL COMMENT '���Ϲ��',
  `materials_source` varchar(100) DEFAULT NULL COMMENT '������Դ',
  `materials_qty` int(12) DEFAULT NULL COMMENT '��������',
  `modulus_no` int(12) DEFAULT NULL COMMENT 'ģ��',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COMMENT='外发加工工序';

-- ----------------------------
-- Records of outsource_order_process
-- ----------------------------

-- ----------------------------
-- Table structure for `outsource_return`
-- ----------------------------
DROP TABLE IF EXISTS `outsource_return`;
CREATE TABLE `outsource_return` (
  `id` int(12) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(2000) DEFAULT '' COMMENT '备注',
  `status` varchar(20) DEFAULT 'draft' COMMENT '状态',
  `approver` varchar(50) DEFAULT NULL COMMENT '审批人',
  `approval_time` datetime DEFAULT NULL COMMENT '审批时间',
  `approval_type` varchar(20) DEFAULT 'N' COMMENT '审批类型',
  `serial_number` varchar(20) NOT NULL COMMENT '单号',
  `supplier_id` int(12) DEFAULT NULL COMMENT '供应商',
  `contact` varchar(50) DEFAULT NULL COMMENT '联系人',
  `cell_phone` varchar(50) DEFAULT NULL COMMENT '手机号',
  `tax_rate` double(12,4) DEFAULT '0.0000' COMMENT '税率',
  `amount` decimal(14,4) DEFAULT '0.0000' COMMENT '总金额',
  `return_date` date DEFAULT NULL COMMENT '退货日期',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='外发退货';

-- ----------------------------
-- Records of outsource_return
-- ----------------------------

-- ----------------------------
-- Table structure for `outsource_return_process`
-- ----------------------------
DROP TABLE IF EXISTS `outsource_return_process`;
CREATE TABLE `outsource_return_process` (
  `id` int(12) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `remark` varchar(2000) DEFAULT '' COMMENT '备注',
  `outsource_return_id` int(12) DEFAULT NULL COMMENT '外发退货',
  `outsource_delivery_id` int(12) DEFAULT NULL COMMENT '外发到货',
  `outsource_delivery_process_id` int(12) DEFAULT NULL COMMENT '外发到货工序',
  `outsource_order_process_id` int(12) DEFAULT NULL COMMENT '外发加工工序',
  `outsource_order_id` int(12) NOT NULL COMMENT '外发加工',
  `sale_order_product_id` int(12) DEFAULT NULL COMMENT '销售产品',
  `sale_order_id` int(12) DEFAULT NULL COMMENT '销售订单',
  `produce_order_id` int(12) DEFAULT NULL COMMENT '施工单',
  `produce_order_process_id` int(12) DEFAULT NULL COMMENT '施工产品',
  `product_id` int(12) DEFAULT NULL COMMENT '产品',
  `process_id` int(12) DEFAULT NULL COMMENT '工序',
  `production_template_id` int(12) DEFAULT NULL COMMENT '生产工艺卡',
  `production_template_process_id` int(12) DEFAULT NULL COMMENT '生产工艺卡工序',
  `outsource_type` varchar(20) DEFAULT NULL COMMENT '外发类型',
  `outsource_valuation_type` varchar(20) DEFAULT NULL COMMENT '计价方式',
  `qty` int(8) DEFAULT '0' COMMENT '数量',
  `price` decimal(14,4) DEFAULT '0.0000' COMMENT '单价',
  `return_type` varchar(20) DEFAULT NULL COMMENT '退货类型',
  `return_rate` double(12,4) DEFAULT '100.0000' COMMENT '退货折扣(%)',
  `tax_rate` double(12,4) DEFAULT '0.0000' COMMENT '税率',
  `amount` decimal(14,4) DEFAULT '0.0000' COMMENT '总金额',
  `return_date` date DEFAULT NULL COMMENT '退货日期',
  `supplier_id` int(12) DEFAULT NULL COMMENT '供应商',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='外发退货工序';

-- ----------------------------
-- Records of outsource_return_process
-- ----------------------------

-- ----------------------------
-- Table structure for `produce_order`
-- ----------------------------
DROP TABLE IF EXISTS `produce_order`;
CREATE TABLE `produce_order` (
  `id` int(12) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(2000) DEFAULT '' COMMENT '备注',
  `status` varchar(20) DEFAULT 'draft' COMMENT '状态',
  `approver` varchar(50) DEFAULT NULL COMMENT '审批人',
  `approval_time` datetime DEFAULT NULL COMMENT '审批时间',
  `approval_type` varchar(20) DEFAULT 'N' COMMENT '审批类型',
  `serial_number` varchar(20) NOT NULL COMMENT '单号',
  `produce_status` varchar(20) DEFAULT NULL COMMENT '生产状态',
  `is_complete` varchar(1) DEFAULT 'N' COMMENT '完工',
  `is_joint` varchar(1) DEFAULT 'N' COMMENT '拼版',
  `is_outsource` varchar(1) DEFAULT 'N' COMMENT '整单外发',
  `produce_order_type` varchar(20) DEFAULT NULL COMMENT '工单类型',
  `delivery_date` date DEFAULT NULL COMMENT '交货日期',
  `produce_date` date DEFAULT NULL COMMENT '生产日期',
  `requirements` varchar(500) DEFAULT NULL COMMENT '生产要求',
  `qty` int(8) DEFAULT '0' COMMENT '生产数量',
  `warehouse_qty` int(8) DEFAULT '0' COMMENT '已入库数量',
  `sale_order_product_id` int(12) DEFAULT NULL COMMENT '销售产品',
  `sale_order_id` int(12) DEFAULT NULL COMMENT '销售订单',
  `product_id` int(12) DEFAULT NULL COMMENT '产品',
  `product_type` varchar(20) DEFAULT 'color' COMMENT '产品类型',
  `customer_id` int(12) DEFAULT NULL COMMENT '客户',
  `customer_material_no` varchar(50) DEFAULT NULL COMMENT '客户料号',
  `production_unit` varchar(20) DEFAULT NULL COMMENT '生产单位',
  `size_long` int(8) DEFAULT '0' COMMENT '产品规格(长)',
  `size_width` int(8) DEFAULT '0' COMMENT '产品规格(宽)',
  `size_height` int(8) DEFAULT '0' COMMENT '产品规格(高)',
  `opensize_long` int(8) DEFAULT '0' COMMENT '展长',
  `opensize_width` int(8) DEFAULT '0' COMMENT '展宽',
  `area` double(12,6) DEFAULT '0.000000' COMMENT '单只面积(m²)',
  `materials_id` int(12) DEFAULT NULL COMMENT '印刷主材',
  `product_img` text COMMENT '产品图片',
  `production_template_id` int(12) DEFAULT NULL COMMENT '生产工艺卡',
  `work_long` int(8) DEFAULT '1092' COMMENT '上机长(mm)',
  `work_width` int(8) DEFAULT '787' COMMENT '上机宽(mm)',
  `is_big_panel` varchar(1) DEFAULT 'N' COMMENT '大色面版',
  `is_special_paper` varchar(1) DEFAULT 'N' COMMENT '特殊纸张',
  `print_sheet_qty` int(8) DEFAULT '1' COMMENT '印张正数',
  `print_sheet_sum_qty` int(8) DEFAULT '1' COMMENT '总印张数',
  `puzzle_qty` int(8) DEFAULT '1' COMMENT '拼版数',
  `forme_qty` int(8) DEFAULT '1' COMMENT '印版付数',
  `print_type` varchar(20) DEFAULT NULL COMMENT '印刷方式',
  `gauge_type` varchar(20) DEFAULT NULL COMMENT '拉规方式',
  `is_buttom` varchar(1) DEFAULT 'Y' COMMENT '是否到底',
  `front_spot` varchar(2000) DEFAULT NULL COMMENT '正面专色',
  `back_spot` varchar(2000) DEFAULT NULL COMMENT '反面专色',
  `front_color` varchar(2000) DEFAULT NULL COMMENT '正面颜色',
  `back_color` varchar(2000) DEFAULT NULL COMMENT '反面颜色',
  `carton_shape_id` int(12) DEFAULT NULL COMMENT '箱型',
  `corrugated_id` int(12) DEFAULT NULL COMMENT '楞型',
  `paper_formula_id` int(12) DEFAULT NULL COMMENT '纸张配方',
  `carton_size_type` varchar(20) DEFAULT NULL COMMENT '生产类型',
  `hole_pattern` varchar(20) DEFAULT NULL COMMENT '孔型',
  `wire_type` varchar(20) DEFAULT NULL COMMENT '压线类型',
  `wire_deep` varchar(20) DEFAULT NULL COMMENT '压线深度',
  `cross_wise` varchar(2000) DEFAULT NULL COMMENT '生产横向压线',
  `vertical_wise` varchar(2000) DEFAULT NULL COMMENT '生产纵向压线',
  `production_size` varchar(2000) DEFAULT NULL COMMENT '生产尺寸',
  `error_range` varchar(2000) DEFAULT NULL COMMENT '误差范围',
  `print_technology_type` varchar(20) DEFAULT NULL COMMENT '印刷工艺',
  `print_times` int(8) DEFAULT '1' COMMENT '印刷次数',
  `plate_qty` int(8) DEFAULT '1' COMMENT '印版数',
  `die_warehouse_id` int(12) DEFAULT NULL COMMENT '原模仓库',
  `die_shelves_no` varchar(50) DEFAULT NULL COMMENT '原模货架号',
  `print_direction` varchar(20) DEFAULT NULL COMMENT '印刷方向',
  `is_aquatone` varchar(1) DEFAULT 'N' COMMENT '是否套色',
  `color_require` varchar(20) DEFAULT NULL COMMENT '颜色要求',
  `charge_amount` decimal(14,4) DEFAULT '0.0000' COMMENT '收费金额',
  `bleed_long_left` int(8) DEFAULT '0' COMMENT '出血位长左',
  `bleed_long_right` int(8) DEFAULT '0' COMMENT '出血位长右',
  `bleed_width_left` int(8) DEFAULT '0' COMMENT '出血位宽左',
  `bleed_width_right` int(8) DEFAULT '0' COMMENT '出血位宽右',
  `extra_cost_info` text COMMENT '���������ϸ',
  `extra_cost` decimal(14,4) DEFAULT '0.0000' COMMENT '额外费用',
  `customer_order` varchar(50) DEFAULT NULL COMMENT '�ͻ�����',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6002 DEFAULT CHARSET=utf8 COMMENT='施工单';

-- ----------------------------
-- Records of produce_order
-- ----------------------------

-- ----------------------------
-- Table structure for `produce_order_materials`
-- ----------------------------
DROP TABLE IF EXISTS `produce_order_materials`;
CREATE TABLE `produce_order_materials` (
  `id` int(12) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `remark` varchar(2000) DEFAULT '' COMMENT '备注',
  `produce_order_product_id` int(12) DEFAULT NULL COMMENT '施工产品',
  `produce_order_id` int(12) DEFAULT NULL COMMENT '施工单',
  `sale_order_product_id` int(12) DEFAULT NULL COMMENT '销售产品',
  `sale_order_id` int(12) DEFAULT NULL COMMENT '销售订单',
  `product_id` int(12) DEFAULT NULL COMMENT '产品',
  `process_id` int(12) DEFAULT NULL COMMENT '关联工序',
  `production_template_id` int(12) DEFAULT NULL COMMENT '生产工艺卡',
  `production_template_process_id` int(12) DEFAULT NULL COMMENT '生产工艺卡工序',
  `materials_id` int(12) DEFAULT NULL COMMENT '材料',
  `cutter_die_id` int(12) DEFAULT NULL COMMENT '刀模',
  `board_id` int(12) DEFAULT NULL COMMENT '板材',
  `is_get_process_qty` varchar(1) DEFAULT 'N' COMMENT '取工序数量',
  `base_rate` double(12,4) DEFAULT '1.0000' COMMENT '比例',
  `loss_rate` double(12,4) DEFAULT '0.0000' COMMENT '损耗率(%)',
  `loss_qty` int(8) DEFAULT '0' COMMENT '固定损耗数',
  `qty` int(8) DEFAULT '0' COMMENT '数量',
  `return_qty` int(8) DEFAULT '0' COMMENT '退料数量',
  `warehouse_id` int(12) DEFAULT NULL COMMENT '仓库',
  `process_size_long` int(8) DEFAULT NULL,
  `process_size_width` int(8) DEFAULT NULL,
  `long_system` varchar(20) DEFAULT NULL,
  `alias` varchar(200) DEFAULT NULL COMMENT '别名',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6141 DEFAULT CHARSET=utf8 COMMENT='施工材料表';

-- ----------------------------
-- Records of produce_order_materials
-- ----------------------------

-- ----------------------------
-- Table structure for `produce_order_part`
-- ----------------------------
DROP TABLE IF EXISTS `produce_order_part`;
CREATE TABLE `produce_order_part` (
  `id` int(12) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `work_long` int(8) DEFAULT '1092' COMMENT '上机长(mm)',
  `work_width` int(8) DEFAULT '787' COMMENT '上机宽(mm)',
  `is_big_panel` varchar(1) DEFAULT 'N' COMMENT '大色面版',
  `is_special_paper` varchar(1) DEFAULT 'N' COMMENT '特殊纸张',
  `print_sheet_qty` int(8) DEFAULT '1' COMMENT '印张正数',
  `print_sheet_sum_qty` int(8) DEFAULT '1' COMMENT '总印张数',
  `puzzle_qty` int(8) DEFAULT '1' COMMENT '拼版数',
  `forme_qty` int(8) DEFAULT '1' COMMENT '印版付数',
  `print_type` varchar(20) DEFAULT NULL COMMENT '印刷方式',
  `gauge_type` varchar(20) DEFAULT NULL COMMENT '拉规方式',
  `is_buttom` varchar(1) DEFAULT 'Y' COMMENT '是否到底',
  `front_spot` varchar(2000) DEFAULT NULL COMMENT '正面专色',
  `back_spot` varchar(2000) DEFAULT NULL COMMENT '反面专色',
  `front_color` varchar(2000) DEFAULT NULL COMMENT '正面颜色',
  `back_color` varchar(2000) DEFAULT NULL COMMENT '反面颜色',
  `carton_shape_id` int(12) DEFAULT NULL COMMENT '箱型',
  `corrugated_id` int(12) DEFAULT NULL COMMENT '楞型',
  `paper_formula_id` int(12) DEFAULT NULL COMMENT '纸张配方',
  `carton_size_type` varchar(20) DEFAULT NULL COMMENT '生产类型',
  `hole_pattern` varchar(20) DEFAULT NULL COMMENT '孔型',
  `wire_type` varchar(20) DEFAULT NULL COMMENT '压线类型',
  `wire_deep` varchar(20) DEFAULT NULL COMMENT '压线深度',
  `cross_wise` varchar(2000) DEFAULT NULL COMMENT '生产横向压线',
  `vertical_wise` varchar(2000) DEFAULT NULL COMMENT '生产纵向压线',
  `production_size` varchar(2000) DEFAULT NULL COMMENT '生产尺寸',
  `error_range` varchar(2000) DEFAULT NULL COMMENT '误差范围',
  `print_technology_type` varchar(20) DEFAULT NULL COMMENT '印刷工艺',
  `print_times` int(8) DEFAULT '1' COMMENT '印刷次数',
  `plate_qty` int(8) DEFAULT '1' COMMENT '印版数',
  `die_warehouse_id` int(12) DEFAULT NULL COMMENT '原模仓库',
  `die_shelves_no` varchar(50) DEFAULT NULL COMMENT '原模货架号',
  `print_direction` varchar(20) DEFAULT NULL COMMENT '印刷方向',
  `is_aquatone` varchar(1) DEFAULT 'N' COMMENT '是否套色',
  `produce_order_id` int(12) DEFAULT NULL COMMENT '父ID',
  `product_name` varchar(50) DEFAULT NULL COMMENT '产品名称',
  `product_type` varchar(20) DEFAULT 'color' COMMENT '彩印/纸箱',
  `product_parent_name` varchar(50) DEFAULT NULL COMMENT '父产品名称',
  `product_id` int(12) DEFAULT NULL COMMENT '产品ID',
  `color_require` varchar(20) DEFAULT NULL COMMENT '颜色要求',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COMMENT='施工单部件';

-- ----------------------------
-- Records of produce_order_part
-- ----------------------------

-- ----------------------------
-- Table structure for `produce_order_process`
-- ----------------------------
DROP TABLE IF EXISTS `produce_order_process`;
CREATE TABLE `produce_order_process` (
  `id` int(12) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `remark` varchar(2000) DEFAULT '' COMMENT '备注',
  `produce_order_id` int(12) DEFAULT NULL COMMENT '施工单',
  `produce_order_product_id` int(12) DEFAULT NULL COMMENT '施工产品',
  `sale_order_product_id` int(12) DEFAULT NULL COMMENT '销售产品',
  `sale_order_id` int(12) DEFAULT NULL COMMENT '销售订单',
  `product_id` int(12) DEFAULT NULL COMMENT '产品',
  `process_id` int(12) DEFAULT NULL COMMENT '工序',
  `production_template_id` int(12) DEFAULT NULL COMMENT '生产工艺卡',
  `production_template_process_id` int(12) DEFAULT NULL COMMENT '生产工艺卡工序',
  `produce_status` varchar(20) DEFAULT 'draft' COMMENT '生产状态',
  `process_order` int(4) DEFAULT '1' COMMENT '顺序',
  `valuation_type` varchar(20) DEFAULT NULL COMMENT '计价方式',
  `is_incount` varchar(1) DEFAULT 'Y' COMMENT '是否计算',
  `qty` int(8) DEFAULT '0' COMMENT '生产数量',
  `is_outsource` varchar(1) DEFAULT 'N' COMMENT '外发',
  `outsource_price` decimal(14,4) DEFAULT '0.0000' COMMENT '外发单价',
  `is_with_materials` varchar(1) DEFAULT 'N' COMMENT '外发带料',
  `times` double(12,4) DEFAULT '1.0000' COMMENT '倍率',
  `price` decimal(14,4) DEFAULT '0.0000' COMMENT '单价',
  `equipment_id` int(12) DEFAULT NULL COMMENT '设备',
  `loss_rate` double(12,4) DEFAULT '0.0000' COMMENT '损耗率(%)',
  `loss_qty` int(8) DEFAULT '0' COMMENT '固定损耗数',
  `requirements` varchar(500) DEFAULT NULL COMMENT '工序要求',
  `in_qty` int(8) DEFAULT '0' COMMENT '投入数',
  `out_qty` int(8) DEFAULT '0' COMMENT '产出数',
  `is_print` varchar(1) DEFAULT 'Y' COMMENT '是否打印',
  `cutting_size_long` int(8) DEFAULT '0' COMMENT '开料尺寸长',
  `cutting_size_width` int(8) DEFAULT '0' COMMENT '开料尺寸宽',
  `alias` varchar(255) DEFAULT NULL,
  `charge_amount` decimal(14,4) DEFAULT '0.0000' COMMENT '收费金额',
  `produce_order_part_id` int(12) DEFAULT NULL COMMENT '部件ID',
  `print_type` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13576 DEFAULT CHARSET=utf8 COMMENT='施工工序表';

-- ----------------------------
-- Records of produce_order_process
-- ----------------------------

-- ----------------------------
-- Table structure for `produce_order_product`
-- ----------------------------
DROP TABLE IF EXISTS `produce_order_product`;
CREATE TABLE `produce_order_product` (
  `id` int(12) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `remark` varchar(2000) DEFAULT '' COMMENT '备注',
  `produce_order_id` int(12) DEFAULT NULL COMMENT '施工单',
  `sale_order_product_id` int(12) DEFAULT NULL COMMENT '销售产品',
  `sale_order_id` int(12) DEFAULT NULL COMMENT '销售订单',
  `product_id` int(12) DEFAULT NULL COMMENT '产品',
  `product_type` varchar(20) DEFAULT 'color' COMMENT '产品类型',
  `customer_id` int(12) DEFAULT NULL COMMENT '客户',
  `customer_material_no` varchar(50) DEFAULT NULL COMMENT '客户料号',
  `production_unit` varchar(20) DEFAULT NULL COMMENT '生产单位',
  `size_long` int(8) DEFAULT '0' COMMENT '产品规格(长)',
  `size_width` int(8) DEFAULT '0' COMMENT '产品规格(宽)',
  `size_height` int(8) DEFAULT '0' COMMENT '产品规格(高)',
  `opensize_long` int(8) DEFAULT '0' COMMENT '展长',
  `opensize_width` int(8) DEFAULT '0' COMMENT '展宽',
  `area` double(12,6) DEFAULT '0.000000' COMMENT '单只面积(m²)',
  `materials_id` int(12) DEFAULT NULL COMMENT '印刷主材',
  `product_img` text COMMENT '产品图片',
  `production_template_id` int(12) DEFAULT NULL COMMENT '生产工艺卡',
  `work_long` int(255) DEFAULT '1092' COMMENT '上机长(mm)',
  `work_width` int(11) DEFAULT '787' COMMENT '上机宽(mm)',
  `is_big_panel` varchar(255) DEFAULT 'N' COMMENT '大色面版',
  `is_special_paper` varchar(255) DEFAULT 'N' COMMENT '特殊纸张',
  `print_sheet_qty` int(11) DEFAULT '1' COMMENT '印张正数',
  `print_sheet_sum_qty` int(11) DEFAULT '1' COMMENT '总印张数',
  `puzzle_qty` int(11) DEFAULT NULL COMMENT '拼版数',
  `forme_qty` int(11) DEFAULT NULL COMMENT '印版付数',
  `print_type` varchar(255) DEFAULT NULL COMMENT '印刷方式',
  `gauge_type` varchar(255) DEFAULT NULL COMMENT '拉规方式',
  `is_buttom` varchar(255) DEFAULT 'Y' COMMENT '是否到底',
  `front_spot` varchar(255) DEFAULT NULL COMMENT '正面专色',
  `back_spot` varchar(255) DEFAULT NULL COMMENT '反面专色',
  `front_color` varchar(255) DEFAULT NULL COMMENT '正面颜色',
  `back_color` varchar(255) DEFAULT NULL COMMENT '反面颜色',
  `carton_shape_id` int(11) DEFAULT NULL COMMENT '箱型',
  `corrugated_id` int(11) DEFAULT NULL COMMENT '楞型',
  `paper_formula_id` int(11) DEFAULT NULL COMMENT '纸张配方',
  `carton_size_type` varchar(255) DEFAULT NULL COMMENT '生产类型',
  `hole_pattern` varchar(255) DEFAULT NULL COMMENT '孔型',
  `wire_type` varchar(255) DEFAULT NULL COMMENT '压线类型',
  `wire_deep` varchar(255) DEFAULT NULL COMMENT '压线深度',
  `cross_wise` varchar(255) DEFAULT NULL COMMENT '生产横向压线',
  `vertical_wise` varchar(255) DEFAULT NULL COMMENT '生产纵向压线',
  `production_size` varchar(255) DEFAULT NULL COMMENT '生产尺寸',
  `error_range` varchar(255) DEFAULT NULL COMMENT '误差范围',
  `print_technology_type` varchar(255) DEFAULT NULL COMMENT '印刷工艺',
  `print_times` int(11) DEFAULT '1' COMMENT '印刷次数',
  `plate_qty` int(11) DEFAULT '1' COMMENT '印版数',
  `die_warehouse_id` int(11) DEFAULT NULL COMMENT '原模仓库',
  `die_shelves_no` varchar(255) DEFAULT NULL COMMENT '原模货架号',
  `print_direction` varchar(255) DEFAULT NULL COMMENT '印刷方向',
  `is_aquatone` varchar(255) DEFAULT 'N' COMMENT '是否套色',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='施工产品表';

-- ----------------------------
-- Records of produce_order_product
-- ----------------------------

-- ----------------------------
-- Table structure for `produce_report`
-- ----------------------------
DROP TABLE IF EXISTS `produce_report`;
CREATE TABLE `produce_report` (
  `id` int(12) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(2000) DEFAULT '' COMMENT '备注',
  `status` varchar(20) DEFAULT 'draft' COMMENT '状态',
  `produce_schedule_process_id` int(12) DEFAULT NULL COMMENT '排程工序',
  `produce_schedule_id` int(12) DEFAULT NULL COMMENT '生产排程',
  `produce_order_process_id` int(12) DEFAULT NULL COMMENT '施工工序',
  `produce_order_product_id` int(12) DEFAULT NULL COMMENT '施工产品',
  `produce_order_id` int(12) DEFAULT NULL COMMENT '施工单',
  `sale_order_product_id` int(12) DEFAULT NULL COMMENT '销售产品',
  `sale_order_id` int(12) DEFAULT NULL COMMENT '销售订单',
  `product_id` int(12) DEFAULT NULL COMMENT '产品',
  `process_id` int(12) DEFAULT NULL COMMENT '工序',
  `qty` int(8) DEFAULT '0' COMMENT '报产数量',
  `report_date` date DEFAULT NULL COMMENT '报产日期',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COMMENT='产量上报';

-- ----------------------------
-- Records of produce_report
-- ----------------------------

-- ----------------------------
-- Table structure for `produce_schedule`
-- ----------------------------
DROP TABLE IF EXISTS `produce_schedule`;
CREATE TABLE `produce_schedule` (
  `id` int(12) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(2000) DEFAULT '' COMMENT '备注',
  `status` varchar(20) DEFAULT 'draft' COMMENT '状态',
  `approver` varchar(50) DEFAULT NULL COMMENT '审批人',
  `approval_time` datetime DEFAULT NULL COMMENT '审批时间',
  `approval_type` varchar(20) DEFAULT 'N' COMMENT '审批类型',
  `serial_number` varchar(20) NOT NULL COMMENT '单号',
  `produce_status` varchar(20) DEFAULT 'draft' COMMENT '生产状态',
  `work_center` varchar(20) DEFAULT NULL COMMENT '工作中心',
  `equipment_id` int(8) DEFAULT NULL COMMENT '设备',
  `team_id` int(8) DEFAULT NULL COMMENT '班组',
  `produce_date` date DEFAULT NULL COMMENT '生产日期',
  `pause_info` text COMMENT '停机',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=60 DEFAULT CHARSET=utf8 COMMENT='生产排程';

-- ----------------------------
-- Records of produce_schedule
-- ----------------------------

-- ----------------------------
-- Table structure for `produce_schedule_process`
-- ----------------------------
DROP TABLE IF EXISTS `produce_schedule_process`;
CREATE TABLE `produce_schedule_process` (
  `id` int(12) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `remark` varchar(2000) DEFAULT '' COMMENT '备注',
  `produce_schedule_id` int(12) DEFAULT NULL COMMENT '生产排程',
  `produce_order_process_id` int(12) DEFAULT NULL COMMENT '施工工序',
  `produce_order_product_id` int(12) DEFAULT NULL COMMENT '施工产品',
  `produce_order_id` int(12) DEFAULT NULL COMMENT '施工单',
  `sale_order_product_id` int(12) DEFAULT NULL COMMENT '销售产品',
  `sale_order_id` int(12) DEFAULT NULL COMMENT '销售订单',
  `production_template_id` int(12) DEFAULT NULL COMMENT '生产工艺卡',
  `production_template_process_id` int(12) DEFAULT NULL COMMENT '生产工艺卡工序',
  `product_id` int(12) DEFAULT NULL COMMENT '产品',
  `process_id` int(12) DEFAULT NULL COMMENT '工序',
  `produce_status` varchar(20) DEFAULT 'draft' COMMENT '生产状态',
  `produce_qty` int(8) DEFAULT '0' COMMENT '生产数量',
  `qty` int(8) DEFAULT '0' COMMENT '报产数量',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=501 DEFAULT CHARSET=utf8 COMMENT='生产排程工序';

-- ----------------------------
-- Records of produce_schedule_process
-- ----------------------------

-- ----------------------------
-- Table structure for `produce_warehouse`
-- ----------------------------
DROP TABLE IF EXISTS `produce_warehouse`;
CREATE TABLE `produce_warehouse` (
  `id` int(12) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(2000) DEFAULT '' COMMENT '备注',
  `status` varchar(20) DEFAULT 'draft' COMMENT '状态',
  `produce_order_id` int(12) DEFAULT NULL COMMENT '施工单',
  `qty` int(8) DEFAULT '0' COMMENT '入库数量',
  `warehouse_date` date DEFAULT NULL COMMENT '入库日期',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='生产入库';

-- ----------------------------
-- Records of produce_warehouse
-- ----------------------------

-- ----------------------------
-- Table structure for `purchase_checking`
-- ----------------------------
DROP TABLE IF EXISTS `purchase_checking`;
CREATE TABLE `purchase_checking` (
  `id` int(12) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(2000) DEFAULT '' COMMENT '备注',
  `status` varchar(20) DEFAULT 'draft' COMMENT '状态',
  `approver` varchar(50) DEFAULT NULL COMMENT '审批人',
  `approval_time` datetime DEFAULT NULL COMMENT '审批时间',
  `approval_type` varchar(20) DEFAULT 'N' COMMENT '审批类型',
  `serial_number` varchar(20) NOT NULL COMMENT '单号',
  `supplier_id` int(12) DEFAULT NULL COMMENT '供应商',
  `checking_date` date DEFAULT NULL COMMENT '对账日期',
  `payment_type` varchar(20) DEFAULT NULL COMMENT '付款方式',
  `settlement_type` varchar(20) DEFAULT NULL COMMENT '结款方式',
  `tax_rate` double(12,4) DEFAULT '0.0000' COMMENT '税率',
  `amount` decimal(14,4) DEFAULT '0.0000' COMMENT '总金额',
  `delivery_amount` decimal(14,4) DEFAULT '0.0000' COMMENT '送货总金额',
  `return_amount` decimal(14,4) DEFAULT '0.0000' COMMENT '退货总金额',
  `invoice_no` varchar(200) DEFAULT NULL COMMENT '发票号',
  `invoice_unit` varchar(200) DEFAULT NULL COMMENT '开票单位',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='采购对账';

-- ----------------------------
-- Records of purchase_checking
-- ----------------------------

-- ----------------------------
-- Table structure for `purchase_checking_materials`
-- ----------------------------
DROP TABLE IF EXISTS `purchase_checking_materials`;
CREATE TABLE `purchase_checking_materials` (
  `id` int(12) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `remark` varchar(2000) DEFAULT '' COMMENT '备注',
  `purchase_checking_id` int(12) DEFAULT NULL COMMENT '销售对账',
  `purchase_order_id` int(12) DEFAULT NULL COMMENT '采购订单',
  `purchase_order_materials_id` int(12) DEFAULT NULL COMMENT '采购订单材料',
  `purchase_delivery_id` int(12) DEFAULT NULL COMMENT '采购到货',
  `purchase_delivery_materials_id` int(12) DEFAULT NULL COMMENT '采购到货材料',
  `purchase_request_id` int(12) DEFAULT NULL COMMENT '采购申请单',
  `produce_order_id` int(12) DEFAULT NULL COMMENT '施工单',
  `produce_order_materials_id` int(12) DEFAULT NULL COMMENT '施工材料',
  `sale_order_id` int(12) DEFAULT NULL COMMENT '销售订单',
  `sale_order_product_id` int(12) DEFAULT NULL COMMENT '销售产品',
  `product_id` int(12) DEFAULT NULL COMMENT '关联产品',
  `process_id` int(12) DEFAULT NULL COMMENT '关联工序',
  `materials_id` int(12) DEFAULT NULL COMMENT '材料',
  `supplier_id` int(12) DEFAULT NULL COMMENT '供应商',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='采购对账材料';

-- ----------------------------
-- Records of purchase_checking_materials
-- ----------------------------

-- ----------------------------
-- Table structure for `purchase_delivery`
-- ----------------------------
DROP TABLE IF EXISTS `purchase_delivery`;
CREATE TABLE `purchase_delivery` (
  `id` int(12) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(2000) DEFAULT '' COMMENT '备注',
  `status` varchar(20) DEFAULT 'draft' COMMENT '状态',
  `approver` varchar(50) DEFAULT NULL COMMENT '审批人',
  `approval_time` datetime DEFAULT NULL COMMENT '审批时间',
  `approval_type` varchar(20) DEFAULT 'N' COMMENT '审批类型',
  `serial_number` varchar(20) NOT NULL COMMENT '单号',
  `supplier_id` int(12) DEFAULT NULL COMMENT '供应商',
  `contact` varchar(50) DEFAULT NULL COMMENT '联系人',
  `cell_phone` varchar(50) DEFAULT NULL COMMENT '手机号',
  `buyer_id` int(12) DEFAULT NULL COMMENT '采购员',
  `delivery_date` date DEFAULT NULL COMMENT '到货日期',
  `purchase_order_type` varchar(20) DEFAULT NULL COMMENT '订单类型',
  `payment_type` varchar(20) DEFAULT NULL COMMENT '付款方式',
  `settlement_type` varchar(20) DEFAULT NULL COMMENT '结款方式',
  `delivery_type` varchar(20) DEFAULT NULL COMMENT '交货方式',
  `tax_rate` double(12,4) DEFAULT '0.0000' COMMENT '税率',
  `amount` decimal(14,4) DEFAULT '0.0000' COMMENT '总金额',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='采购到货';

-- ----------------------------
-- Records of purchase_delivery
-- ----------------------------

-- ----------------------------
-- Table structure for `purchase_delivery_materials`
-- ----------------------------
DROP TABLE IF EXISTS `purchase_delivery_materials`;
CREATE TABLE `purchase_delivery_materials` (
  `id` int(12) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `remark` varchar(2000) DEFAULT '' COMMENT '备注',
  `purchase_delivery_id` int(12) DEFAULT NULL COMMENT '采购到货',
  `purchase_order_id` int(12) DEFAULT NULL COMMENT '采购订单',
  `purchase_order_materials_id` int(12) DEFAULT NULL COMMENT '订单材料',
  `produce_order_id` int(12) DEFAULT NULL COMMENT '施工单',
  `produce_order_materials_id` int(12) DEFAULT NULL COMMENT '施工材料',
  `sale_order_id` int(12) DEFAULT NULL COMMENT '销售订单',
  `sale_order_product_id` int(12) DEFAULT NULL COMMENT '销售产品',
  `purchase_request_id` int(12) DEFAULT NULL COMMENT '采购申请单',
  `product_id` int(12) DEFAULT NULL COMMENT '关联产品',
  `process_id` int(12) DEFAULT NULL COMMENT '关联工序',
  `materials_id` int(12) DEFAULT NULL COMMENT '材料',
  `supplier_id` int(12) DEFAULT NULL COMMENT '供应商',
  `warehouse_id` int(12) DEFAULT NULL COMMENT '仓库',
  `org_price` decimal(14,4) DEFAULT '0.0000' COMMENT '订单单价',
  `price` decimal(14,4) DEFAULT '0.0000' COMMENT '单价',
  `size_long` int(8) DEFAULT '0' COMMENT '规格长',
  `size_width` int(8) DEFAULT '0' COMMENT '规格宽',
  `qty` int(8) DEFAULT '0' COMMENT '数量',
  `return_qty` int(8) DEFAULT '0' COMMENT '退货数量',
  `tax_rate` double(12,4) DEFAULT '0.0000' COMMENT '税率',
  `amount` decimal(14,4) DEFAULT '0.0000' COMMENT '总金额',
  `delivery_date` date DEFAULT NULL COMMENT '到货日期',
  `requirements` varchar(500) DEFAULT NULL COMMENT '材料要求',
  `address` varchar(500) DEFAULT NULL COMMENT '送货地址',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='采购到货材料';

-- ----------------------------
-- Records of purchase_delivery_materials
-- ----------------------------

-- ----------------------------
-- Table structure for `purchase_order`
-- ----------------------------
DROP TABLE IF EXISTS `purchase_order`;
CREATE TABLE `purchase_order` (
  `id` int(12) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(2000) DEFAULT '' COMMENT '备注',
  `status` varchar(20) DEFAULT 'draft' COMMENT '状态',
  `approver` varchar(50) DEFAULT NULL COMMENT '审批人',
  `approval_time` datetime DEFAULT NULL COMMENT '审批时间',
  `approval_type` varchar(20) DEFAULT 'N' COMMENT '审批类型',
  `serial_number` varchar(20) NOT NULL COMMENT '单号',
  `supplier_id` int(12) DEFAULT NULL COMMENT '供应商',
  `contact` varchar(50) DEFAULT NULL COMMENT '联系人',
  `cell_phone` varchar(50) DEFAULT NULL COMMENT '手机号',
  `buyer_id` int(12) DEFAULT NULL COMMENT '采购员',
  `delivery_date` date DEFAULT NULL COMMENT '到货日期',
  `purchase_order_type` varchar(20) DEFAULT NULL COMMENT '订单类型',
  `payment_type` varchar(20) DEFAULT NULL COMMENT '付款方式',
  `settlement_type` varchar(20) DEFAULT NULL COMMENT '结款方式',
  `delivery_type` varchar(20) DEFAULT NULL COMMENT '交货方式',
  `tax_rate` double(12,4) DEFAULT '0.0000' COMMENT '税率',
  `amount` decimal(14,4) DEFAULT '0.0000' COMMENT '总金额',
  `deposit` decimal(14,4) DEFAULT '0.0000' COMMENT '订金',
  `deposit_date` date DEFAULT NULL COMMENT '订金日期',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COMMENT='采购订单';

-- ----------------------------
-- Records of purchase_order
-- ----------------------------

-- ----------------------------
-- Table structure for `purchase_order_materials`
-- ----------------------------
DROP TABLE IF EXISTS `purchase_order_materials`;
CREATE TABLE `purchase_order_materials` (
  `id` int(12) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `remark` varchar(2000) DEFAULT '' COMMENT '备注',
  `purchase_order_id` int(12) DEFAULT NULL COMMENT '采购订单',
  `purchase_request_id` int(12) DEFAULT NULL COMMENT '采购申请单',
  `sale_order_id` int(12) DEFAULT NULL COMMENT '销售订单',
  `sale_order_product_id` int(12) DEFAULT NULL COMMENT '销售产品',
  `produce_order_id` int(12) DEFAULT NULL COMMENT '施工单',
  `produce_order_materials_id` int(12) DEFAULT NULL COMMENT '施工材料',
  `product_id` int(12) DEFAULT NULL COMMENT '关联产品',
  `process_id` int(12) DEFAULT NULL COMMENT '关联工序',
  `materials_id` int(12) DEFAULT NULL COMMENT '材料',
  `supplier_id` int(12) DEFAULT NULL COMMENT '供应商',
  `price` decimal(14,4) DEFAULT '0.0000' COMMENT '单价',
  `size_long` int(8) DEFAULT '0' COMMENT '规格长',
  `size_width` int(8) DEFAULT '0' COMMENT '规格宽',
  `qty` int(8) DEFAULT '0' COMMENT '数量',
  `tax_rate` double(12,4) DEFAULT '0.0000' COMMENT '税率',
  `amount` decimal(14,4) DEFAULT '0.0000' COMMENT '总金额',
  `delivery_date` date DEFAULT NULL COMMENT '到货日期',
  `requirements` varchar(255) DEFAULT NULL COMMENT '材料要求',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COMMENT='采购订单材料';

-- ----------------------------
-- Records of purchase_order_materials
-- ----------------------------

-- ----------------------------
-- Table structure for `purchase_request`
-- ----------------------------
DROP TABLE IF EXISTS `purchase_request`;
CREATE TABLE `purchase_request` (
  `id` int(12) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(2000) DEFAULT '' COMMENT '备注',
  `status` varchar(20) DEFAULT 'draft' COMMENT '状态',
  `approver` varchar(50) DEFAULT NULL COMMENT '审批人',
  `approval_time` datetime DEFAULT NULL COMMENT '审批时间',
  `approval_type` varchar(20) DEFAULT 'N' COMMENT '审批类型',
  `serial_number` varchar(20) NOT NULL COMMENT '单号',
  `sale_order_id` int(12) DEFAULT NULL COMMENT '销售订单',
  `sale_order_product_id` int(12) DEFAULT NULL COMMENT '销售产品',
  `produce_order_id` int(12) DEFAULT NULL COMMENT '施工单',
  `produce_order_materials_id` int(12) DEFAULT NULL COMMENT '施工材料',
  `product_id` int(12) DEFAULT NULL COMMENT '关联产品',
  `process_id` int(12) DEFAULT NULL COMMENT '关联工序',
  `supplier_id` int(12) DEFAULT NULL COMMENT '供应商',
  `contact` varchar(50) DEFAULT NULL COMMENT '联系人',
  `cell_phone` varchar(50) DEFAULT NULL COMMENT '手机号',
  `materials_id` int(12) DEFAULT NULL COMMENT '材料',
  `size_long` int(8) DEFAULT '0' COMMENT '材料规格长',
  `size_width` int(8) DEFAULT '0' COMMENT '材料规格宽',
  `qty` int(8) DEFAULT '0' COMMENT '数量',
  `requirements` varchar(500) DEFAULT NULL COMMENT '材料要求',
  `delivery_date` date DEFAULT NULL COMMENT '期望到货日期',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8 COMMENT='采购申请';

-- ----------------------------
-- Records of purchase_request
-- ----------------------------

-- ----------------------------
-- Table structure for `purchase_return`
-- ----------------------------
DROP TABLE IF EXISTS `purchase_return`;
CREATE TABLE `purchase_return` (
  `id` int(12) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(2000) DEFAULT '' COMMENT '备注',
  `status` varchar(20) DEFAULT 'draft' COMMENT '状态',
  `approver` varchar(50) DEFAULT NULL COMMENT '审批人',
  `approval_time` datetime DEFAULT NULL COMMENT '审批时间',
  `approval_type` varchar(20) DEFAULT 'N' COMMENT '审批类型',
  `serial_number` varchar(20) NOT NULL COMMENT '单号',
  `supplier_id` int(12) DEFAULT NULL COMMENT '供应商',
  `contact` varchar(50) DEFAULT NULL COMMENT '联系人',
  `cell_phone` varchar(50) DEFAULT NULL COMMENT '手机号',
  `buyer_id` int(12) DEFAULT NULL COMMENT '采购员',
  `return_date` date DEFAULT NULL COMMENT '退货日期',
  `purchase_order_type` varchar(20) DEFAULT NULL COMMENT '订单类型',
  `payment_type` varchar(20) DEFAULT NULL COMMENT '付款方式',
  `settlement_type` varchar(20) DEFAULT NULL COMMENT '结款方式',
  `delivery_type` varchar(20) DEFAULT NULL COMMENT '交货方式',
  `tax_rate` double(12,4) DEFAULT '0.0000' COMMENT '税率',
  `amount` decimal(14,4) DEFAULT '0.0000' COMMENT '总金额',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='采购退货';

-- ----------------------------
-- Records of purchase_return
-- ----------------------------

-- ----------------------------
-- Table structure for `purchase_return_materials`
-- ----------------------------
DROP TABLE IF EXISTS `purchase_return_materials`;
CREATE TABLE `purchase_return_materials` (
  `id` int(12) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `remark` varchar(2000) DEFAULT '' COMMENT '备注',
  `purchase_return_id` int(12) DEFAULT NULL COMMENT '采购退货',
  `purchase_delivery_id` int(12) DEFAULT NULL COMMENT '采购到货',
  `purchase_delivery_materials_id` int(12) DEFAULT NULL COMMENT '采购到货材料',
  `purchase_order_id` int(12) DEFAULT NULL COMMENT '采购订单',
  `produce_order_id` int(12) DEFAULT NULL COMMENT '施工单',
  `produce_order_materials_id` int(12) DEFAULT NULL COMMENT '施工材料',
  `purchase_request_id` int(12) DEFAULT NULL COMMENT '采购申请单',
  `sale_order_id` int(12) DEFAULT NULL COMMENT '销售订单',
  `sale_order_product_id` int(12) DEFAULT NULL COMMENT '销售产品',
  `product_id` int(12) DEFAULT NULL COMMENT '关联产品',
  `process_id` int(12) DEFAULT NULL COMMENT '关联工序',
  `materials_id` int(12) DEFAULT NULL COMMENT '材料',
  `supplier_id` int(12) DEFAULT NULL COMMENT '供应商',
  `warehouse_id` int(12) DEFAULT NULL COMMENT '仓库',
  `return_rate` double(12,4) DEFAULT '100.0000' COMMENT '退货折扣(%)',
  `return_type` varchar(20) DEFAULT 'THKK' COMMENT '退货类型',
  `return_date` date DEFAULT NULL COMMENT '退货日期',
  `price` decimal(14,4) DEFAULT '0.0000' COMMENT '单价',
  `size_long` int(8) DEFAULT '0' COMMENT '规格长',
  `size_width` int(8) DEFAULT '0' COMMENT '规格宽',
  `qty` int(8) DEFAULT '0' COMMENT '数量',
  `return_qty` int(8) DEFAULT '0' COMMENT '退货数量',
  `tax_rate` double(12,4) DEFAULT '0.0000' COMMENT '税率',
  `amount` decimal(14,4) DEFAULT '0.0000' COMMENT '总金额',
  `requirements` varchar(500) DEFAULT NULL COMMENT '材料要求',
  `address` varchar(500) DEFAULT NULL COMMENT '退货地址',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='采购退货材料';

-- ----------------------------
-- Records of purchase_return_materials
-- ----------------------------

-- ----------------------------
-- Table structure for `qrtz_blob_triggers`
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_blob_triggers`;
CREATE TABLE `qrtz_blob_triggers` (
  `sched_name` varchar(120) NOT NULL,
  `trigger_name` varchar(200) NOT NULL,
  `trigger_group` varchar(200) NOT NULL,
  `blob_data` blob,
  PRIMARY KEY (`sched_name`,`trigger_name`,`trigger_group`),
  CONSTRAINT `qrtz_blob_triggers_ibfk_1` FOREIGN KEY (`sched_name`, `trigger_name`, `trigger_group`) REFERENCES `qrtz_triggers` (`sched_name`, `trigger_name`, `trigger_group`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of qrtz_blob_triggers
-- ----------------------------

-- ----------------------------
-- Table structure for `qrtz_calendars`
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_calendars`;
CREATE TABLE `qrtz_calendars` (
  `sched_name` varchar(120) NOT NULL,
  `calendar_name` varchar(200) NOT NULL,
  `calendar` blob NOT NULL,
  PRIMARY KEY (`sched_name`,`calendar_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of qrtz_calendars
-- ----------------------------

-- ----------------------------
-- Table structure for `qrtz_cron_triggers`
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_cron_triggers`;
CREATE TABLE `qrtz_cron_triggers` (
  `sched_name` varchar(120) NOT NULL,
  `trigger_name` varchar(200) NOT NULL,
  `trigger_group` varchar(200) NOT NULL,
  `cron_expression` varchar(200) NOT NULL,
  `time_zone_id` varchar(80) DEFAULT NULL,
  PRIMARY KEY (`sched_name`,`trigger_name`,`trigger_group`),
  CONSTRAINT `qrtz_cron_triggers_ibfk_1` FOREIGN KEY (`sched_name`, `trigger_name`, `trigger_group`) REFERENCES `qrtz_triggers` (`sched_name`, `trigger_name`, `trigger_group`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of qrtz_cron_triggers
-- ----------------------------

-- ----------------------------
-- Table structure for `qrtz_fired_triggers`
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_fired_triggers`;
CREATE TABLE `qrtz_fired_triggers` (
  `sched_name` varchar(120) NOT NULL,
  `entry_id` varchar(95) NOT NULL,
  `trigger_name` varchar(200) NOT NULL,
  `trigger_group` varchar(200) NOT NULL,
  `instance_name` varchar(200) NOT NULL,
  `fired_time` bigint(13) NOT NULL,
  `sched_time` bigint(13) NOT NULL,
  `priority` int(11) NOT NULL,
  `state` varchar(16) NOT NULL,
  `job_name` varchar(200) DEFAULT NULL,
  `job_group` varchar(200) DEFAULT NULL,
  `is_nonconcurrent` varchar(1) DEFAULT NULL,
  `requests_recovery` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`sched_name`,`entry_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of qrtz_fired_triggers
-- ----------------------------

-- ----------------------------
-- Table structure for `qrtz_job_details`
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_job_details`;
CREATE TABLE `qrtz_job_details` (
  `sched_name` varchar(120) NOT NULL,
  `job_name` varchar(200) NOT NULL,
  `job_group` varchar(200) NOT NULL,
  `description` varchar(250) DEFAULT NULL,
  `job_class_name` varchar(250) NOT NULL,
  `is_durable` varchar(1) NOT NULL,
  `is_nonconcurrent` varchar(1) NOT NULL,
  `is_update_data` varchar(1) NOT NULL,
  `requests_recovery` varchar(1) NOT NULL,
  `job_data` blob,
  PRIMARY KEY (`sched_name`,`job_name`,`job_group`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of qrtz_job_details
-- ----------------------------
INSERT INTO `qrtz_job_details` VALUES ('RuoyiScheduler', 'TASK_CLASS_NAME1', 'DEFAULT', null, 'com.ruoyi.project.monitor.job.util.QuartzDisallowConcurrentExecution', '0', '1', '0', '0', 0xACED0005737200156F72672E71756172747A2E4A6F62446174614D61709FB083E8BFA9B0CB020000787200266F72672E71756172747A2E7574696C732E537472696E674B65794469727479466C61674D61708208E8C3FBC55D280200015A0013616C6C6F77735472616E7369656E74446174617872001D6F72672E71756172747A2E7574696C732E4469727479466C61674D617013E62EAD28760ACE0200025A000564697274794C00036D617074000F4C6A6176612F7574696C2F4D61703B787001737200116A6176612E7574696C2E486173684D61700507DAC1C31660D103000246000A6C6F6164466163746F724900097468726573686F6C6478703F4000000000000C7708000000100000000174000F5441534B5F50524F5045525449455373720028636F6D2E72756F79692E70726F6A6563742E6D6F6E69746F722E6A6F622E646F6D61696E2E4A6F6200000000000000010200084C000A636F6E63757272656E747400124C6A6176612F6C616E672F537472696E673B4C000E63726F6E45787072657373696F6E71007E00094C000C696E766F6B6554617267657471007E00094C00086A6F6247726F757071007E00094C00056A6F6249647400104C6A6176612F6C616E672F4C6F6E673B4C00076A6F624E616D6571007E00094C000D6D697366697265506F6C69637971007E00094C000673746174757371007E000978720029636F6D2E72756F79692E6672616D65776F726B2E7765622E646F6D61696E2E42617365456E74697479000000000000000102000D4C0008637265617465427971007E00094C000A63726561746554696D657400104C6A6176612F7574696C2F446174653B4C000369647371007E00094C0006706172616D7371007E00034C000672656D61726B71007E00094C000B727A79437573746F6D657271007E00094C0009727A7955706461746571007E00094C0009727A7955736572496471007E000A4C0009727A7956616C75653171007E00094C0009727A7956616C75653271007E00094C000B73656172636856616C756571007E00094C0008757064617465427971007E00094C000A75706461746554696D6571007E000C787074000561646D696E7372000E6A6176612E7574696C2E44617465686A81014B5974190300007870770800000178C43672F078707074000074000071007E00127070707070707400013174000E302F3130202A202A202A202A203F74001172795461736B2E72794E6F506172616D7374000744454641554C547372000E6A6176612E6C616E672E4C6F6E673B8BE490CC8F23DF0200014A000576616C7565787200106A6176612E6C616E672E4E756D62657286AC951D0B94E08B02000078700000000000000001740018E7B3BBE7BB9FE9BB98E8AEA4EFBC88E697A0E58F82EFBC8974000133740001317800);
INSERT INTO `qrtz_job_details` VALUES ('RuoyiScheduler', 'TASK_CLASS_NAME2', 'DEFAULT', null, 'com.ruoyi.project.monitor.job.util.QuartzDisallowConcurrentExecution', '0', '1', '0', '0', 0xACED0005737200156F72672E71756172747A2E4A6F62446174614D61709FB083E8BFA9B0CB020000787200266F72672E71756172747A2E7574696C732E537472696E674B65794469727479466C61674D61708208E8C3FBC55D280200015A0013616C6C6F77735472616E7369656E74446174617872001D6F72672E71756172747A2E7574696C732E4469727479466C61674D617013E62EAD28760ACE0200025A000564697274794C00036D617074000F4C6A6176612F7574696C2F4D61703B787001737200116A6176612E7574696C2E486173684D61700507DAC1C31660D103000246000A6C6F6164466163746F724900097468726573686F6C6478703F4000000000000C7708000000100000000174000F5441534B5F50524F5045525449455373720028636F6D2E72756F79692E70726F6A6563742E6D6F6E69746F722E6A6F622E646F6D61696E2E4A6F6200000000000000010200084C000A636F6E63757272656E747400124C6A6176612F6C616E672F537472696E673B4C000E63726F6E45787072657373696F6E71007E00094C000C696E766F6B6554617267657471007E00094C00086A6F6247726F757071007E00094C00056A6F6249647400104C6A6176612F6C616E672F4C6F6E673B4C00076A6F624E616D6571007E00094C000D6D697366697265506F6C69637971007E00094C000673746174757371007E000978720029636F6D2E72756F79692E6672616D65776F726B2E7765622E646F6D61696E2E42617365456E74697479000000000000000102000D4C0008637265617465427971007E00094C000A63726561746554696D657400104C6A6176612F7574696C2F446174653B4C000369647371007E00094C0006706172616D7371007E00034C000672656D61726B71007E00094C000B727A79437573746F6D657271007E00094C0009727A7955706461746571007E00094C0009727A7955736572496471007E000A4C0009727A7956616C75653171007E00094C0009727A7956616C75653271007E00094C000B73656172636856616C756571007E00094C0008757064617465427971007E00094C000A75706461746554696D6571007E000C787074000561646D696E7372000E6A6176612E7574696C2E44617465686A81014B5974190300007870770800000178C43672F078707074000074000071007E00127070707070707400013174000E302F3135202A202A202A202A203F74001572795461736B2E7279506172616D7328277279272974000744454641554C547372000E6A6176612E6C616E672E4C6F6E673B8BE490CC8F23DF0200014A000576616C7565787200106A6176612E6C616E672E4E756D62657286AC951D0B94E08B02000078700000000000000002740018E7B3BBE7BB9FE9BB98E8AEA4EFBC88E69C89E58F82EFBC8974000133740001317800);
INSERT INTO `qrtz_job_details` VALUES ('RuoyiScheduler', 'TASK_CLASS_NAME3', 'DEFAULT', null, 'com.ruoyi.project.monitor.job.util.QuartzDisallowConcurrentExecution', '0', '1', '0', '0', 0xACED0005737200156F72672E71756172747A2E4A6F62446174614D61709FB083E8BFA9B0CB020000787200266F72672E71756172747A2E7574696C732E537472696E674B65794469727479466C61674D61708208E8C3FBC55D280200015A0013616C6C6F77735472616E7369656E74446174617872001D6F72672E71756172747A2E7574696C732E4469727479466C61674D617013E62EAD28760ACE0200025A000564697274794C00036D617074000F4C6A6176612F7574696C2F4D61703B787001737200116A6176612E7574696C2E486173684D61700507DAC1C31660D103000246000A6C6F6164466163746F724900097468726573686F6C6478703F4000000000000C7708000000100000000174000F5441534B5F50524F5045525449455373720028636F6D2E72756F79692E70726F6A6563742E6D6F6E69746F722E6A6F622E646F6D61696E2E4A6F6200000000000000010200084C000A636F6E63757272656E747400124C6A6176612F6C616E672F537472696E673B4C000E63726F6E45787072657373696F6E71007E00094C000C696E766F6B6554617267657471007E00094C00086A6F6247726F757071007E00094C00056A6F6249647400104C6A6176612F6C616E672F4C6F6E673B4C00076A6F624E616D6571007E00094C000D6D697366697265506F6C69637971007E00094C000673746174757371007E000978720029636F6D2E72756F79692E6672616D65776F726B2E7765622E646F6D61696E2E42617365456E74697479000000000000000102000D4C0008637265617465427971007E00094C000A63726561746554696D657400104C6A6176612F7574696C2F446174653B4C000369647371007E00094C0006706172616D7371007E00034C000672656D61726B71007E00094C000B727A79437573746F6D657271007E00094C0009727A7955706461746571007E00094C0009727A7955736572496471007E000A4C0009727A7956616C75653171007E00094C0009727A7956616C75653271007E00094C000B73656172636856616C756571007E00094C0008757064617465427971007E00094C000A75706461746554696D6571007E000C787074000561646D696E7372000E6A6176612E7574696C2E44617465686A81014B5974190300007870770800000178C43672F078707074000074000071007E00127070707070707400013174000E302F3230202A202A202A202A203F74003872795461736B2E72794D756C7469706C65506172616D7328277279272C20747275652C20323030304C2C203331362E3530442C203130302974000744454641554C547372000E6A6176612E6C616E672E4C6F6E673B8BE490CC8F23DF0200014A000576616C7565787200106A6176612E6C616E672E4E756D62657286AC951D0B94E08B02000078700000000000000003740018E7B3BBE7BB9FE9BB98E8AEA4EFBC88E5A49AE58F82EFBC8974000133740001317800);

-- ----------------------------
-- Table structure for `qrtz_locks`
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_locks`;
CREATE TABLE `qrtz_locks` (
  `sched_name` varchar(120) NOT NULL,
  `lock_name` varchar(40) NOT NULL,
  PRIMARY KEY (`sched_name`,`lock_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of qrtz_locks
-- ----------------------------

-- ----------------------------
-- Table structure for `qrtz_paused_trigger_grps`
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_paused_trigger_grps`;
CREATE TABLE `qrtz_paused_trigger_grps` (
  `sched_name` varchar(120) NOT NULL,
  `trigger_group` varchar(200) NOT NULL,
  PRIMARY KEY (`sched_name`,`trigger_group`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of qrtz_paused_trigger_grps
-- ----------------------------

-- ----------------------------
-- Table structure for `qrtz_scheduler_state`
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_scheduler_state`;
CREATE TABLE `qrtz_scheduler_state` (
  `sched_name` varchar(120) NOT NULL,
  `instance_name` varchar(200) NOT NULL,
  `last_checkin_time` bigint(13) NOT NULL,
  `checkin_interval` bigint(13) NOT NULL,
  PRIMARY KEY (`sched_name`,`instance_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of qrtz_scheduler_state
-- ----------------------------

-- ----------------------------
-- Table structure for `qrtz_simple_triggers`
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_simple_triggers`;
CREATE TABLE `qrtz_simple_triggers` (
  `sched_name` varchar(120) NOT NULL,
  `trigger_name` varchar(200) NOT NULL,
  `trigger_group` varchar(200) NOT NULL,
  `repeat_count` bigint(7) NOT NULL,
  `repeat_interval` bigint(12) NOT NULL,
  `times_triggered` bigint(10) NOT NULL,
  PRIMARY KEY (`sched_name`,`trigger_name`,`trigger_group`),
  CONSTRAINT `qrtz_simple_triggers_ibfk_1` FOREIGN KEY (`sched_name`, `trigger_name`, `trigger_group`) REFERENCES `qrtz_triggers` (`sched_name`, `trigger_name`, `trigger_group`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of qrtz_simple_triggers
-- ----------------------------

-- ----------------------------
-- Table structure for `qrtz_simprop_triggers`
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_simprop_triggers`;
CREATE TABLE `qrtz_simprop_triggers` (
  `sched_name` varchar(120) NOT NULL,
  `trigger_name` varchar(200) NOT NULL,
  `trigger_group` varchar(200) NOT NULL,
  `str_prop_1` varchar(512) DEFAULT NULL,
  `str_prop_2` varchar(512) DEFAULT NULL,
  `str_prop_3` varchar(512) DEFAULT NULL,
  `int_prop_1` int(11) DEFAULT NULL,
  `int_prop_2` int(11) DEFAULT NULL,
  `long_prop_1` bigint(20) DEFAULT NULL,
  `long_prop_2` bigint(20) DEFAULT NULL,
  `dec_prop_1` decimal(13,4) DEFAULT NULL,
  `dec_prop_2` decimal(13,4) DEFAULT NULL,
  `bool_prop_1` varchar(1) DEFAULT NULL,
  `bool_prop_2` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`sched_name`,`trigger_name`,`trigger_group`),
  CONSTRAINT `qrtz_simprop_triggers_ibfk_1` FOREIGN KEY (`sched_name`, `trigger_name`, `trigger_group`) REFERENCES `qrtz_triggers` (`sched_name`, `trigger_name`, `trigger_group`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of qrtz_simprop_triggers
-- ----------------------------

-- ----------------------------
-- Table structure for `qrtz_triggers`
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_triggers`;
CREATE TABLE `qrtz_triggers` (
  `sched_name` varchar(120) NOT NULL,
  `trigger_name` varchar(200) NOT NULL,
  `trigger_group` varchar(200) NOT NULL,
  `job_name` varchar(200) NOT NULL,
  `job_group` varchar(200) NOT NULL,
  `description` varchar(250) DEFAULT NULL,
  `next_fire_time` bigint(13) DEFAULT NULL,
  `prev_fire_time` bigint(13) DEFAULT NULL,
  `priority` int(11) DEFAULT NULL,
  `trigger_state` varchar(16) NOT NULL,
  `trigger_type` varchar(8) NOT NULL,
  `start_time` bigint(13) NOT NULL,
  `end_time` bigint(13) DEFAULT NULL,
  `calendar_name` varchar(200) DEFAULT NULL,
  `misfire_instr` smallint(2) DEFAULT NULL,
  `job_data` blob,
  PRIMARY KEY (`sched_name`,`trigger_name`,`trigger_group`),
  KEY `sched_name` (`sched_name`,`job_name`,`job_group`),
  CONSTRAINT `qrtz_triggers_ibfk_1` FOREIGN KEY (`sched_name`, `job_name`, `job_group`) REFERENCES `qrtz_job_details` (`sched_name`, `job_name`, `job_group`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of qrtz_triggers
-- ----------------------------

-- ----------------------------
-- Table structure for `report_common`
-- ----------------------------
DROP TABLE IF EXISTS `report_common`;
CREATE TABLE `report_common` (
  `id` int(12) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(2000) DEFAULT '' COMMENT '备注',
  `status` varchar(20) DEFAULT 'vaild' COMMENT '状态',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='报表基础';

-- ----------------------------
-- Records of report_common
-- ----------------------------

-- ----------------------------
-- Table structure for `rzy_common`
-- ----------------------------
DROP TABLE IF EXISTS `rzy_common`;
CREATE TABLE `rzy_common` (
  `str_value1` varchar(200) DEFAULT NULL COMMENT '字符串1',
  `str_value2` varchar(200) DEFAULT NULL COMMENT '字符串2',
  `str_value3` varchar(200) DEFAULT NULL COMMENT '字符串3',
  `str_value4` varchar(200) DEFAULT NULL COMMENT '字符串4',
  `str_value5` varchar(200) DEFAULT NULL COMMENT '字符串5',
  `str_value6` varchar(200) DEFAULT NULL COMMENT '字符串6',
  `str_value7` varchar(200) DEFAULT NULL COMMENT '字符串7',
  `str_value8` varchar(200) DEFAULT NULL COMMENT '字符串8',
  `str_value9` varchar(200) DEFAULT NULL COMMENT '字符串9',
  `int_value1` int(8) DEFAULT NULL COMMENT '数值1',
  `int_value2` int(8) DEFAULT NULL COMMENT '数值2',
  `int_value3` int(8) DEFAULT NULL COMMENT '数值3',
  `int_value4` int(8) DEFAULT NULL COMMENT '数值4',
  `int_value5` int(8) DEFAULT NULL COMMENT '数值5',
  `int_value6` int(8) DEFAULT NULL COMMENT '数值6',
  `int_value7` int(8) DEFAULT NULL COMMENT '数值7',
  `int_value8` int(8) DEFAULT NULL COMMENT '数值8',
  `int_value9` int(8) DEFAULT NULL COMMENT '数值9',
  `double_value1` double(12,4) DEFAULT NULL COMMENT '浮点数1',
  `double_value2` double(12,4) DEFAULT NULL COMMENT '浮点数2',
  `double_value3` double(12,4) DEFAULT NULL COMMENT '浮点数3',
  `double_value4` double(12,4) DEFAULT NULL COMMENT '浮点数4',
  `double_value5` double(12,4) DEFAULT NULL COMMENT '浮点数5',
  `date_value1` date DEFAULT NULL COMMENT '日期1',
  `date_value2` date DEFAULT NULL COMMENT '日期2',
  `date_value3` date DEFAULT NULL COMMENT '日期3',
  `date_value4` date DEFAULT NULL COMMENT '日期4',
  `date_value5` date DEFAULT NULL COMMENT '日期5'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='虚拟公共表';

-- ----------------------------
-- Records of rzy_common
-- ----------------------------

-- ----------------------------
-- Table structure for `sale_checking`
-- ----------------------------
DROP TABLE IF EXISTS `sale_checking`;
CREATE TABLE `sale_checking` (
  `id` int(12) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(2000) DEFAULT '' COMMENT '备注',
  `status` varchar(20) DEFAULT 'draft' COMMENT '状态',
  `approver` varchar(50) DEFAULT NULL COMMENT '审批人',
  `approval_time` datetime DEFAULT NULL COMMENT '审批时间',
  `approval_type` varchar(20) DEFAULT 'N' COMMENT '审批类型',
  `serial_number` varchar(20) NOT NULL COMMENT '单号',
  `customer_id` int(12) DEFAULT NULL COMMENT '客户',
  `checking_date` date DEFAULT NULL COMMENT '对账日期',
  `payment_type` varchar(20) DEFAULT NULL COMMENT '付款方式',
  `settlement_type` varchar(20) DEFAULT NULL COMMENT '结款方式',
  `tax_rate` double(12,4) DEFAULT '0.0000' COMMENT '税率',
  `amount` decimal(14,4) DEFAULT '0.0000' COMMENT '总金额',
  `delivery_amount` decimal(14,4) DEFAULT '0.0000' COMMENT '送货总金额',
  `return_amount` decimal(14,4) DEFAULT '0.0000' COMMENT '退货总金额',
  `invoice_no` varchar(200) DEFAULT NULL COMMENT '发票号',
  `invoice_unit` varchar(200) DEFAULT NULL COMMENT '开票单位',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COMMENT='销售对账';

-- ----------------------------
-- Records of sale_checking
-- ----------------------------

-- ----------------------------
-- Table structure for `sale_checking_product`
-- ----------------------------
DROP TABLE IF EXISTS `sale_checking_product`;
CREATE TABLE `sale_checking_product` (
  `id` int(12) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `remark` varchar(2000) DEFAULT '' COMMENT '备注',
  `sale_checking_id` int(12) DEFAULT NULL COMMENT '销售对账',
  `sale_order_id` int(12) DEFAULT NULL COMMENT '销售订单',
  `sale_order_product_id` int(12) DEFAULT NULL COMMENT '销售产品',
  `sale_order_delivery_id` int(12) DEFAULT NULL COMMENT '送货排程',
  `sale_delivery_id` int(12) DEFAULT NULL COMMENT '送货单',
  `sale_delivery_product_id` int(12) DEFAULT NULL COMMENT '送货产品',
  `customer_id` int(12) DEFAULT NULL COMMENT '客户',
  `product_id` int(12) DEFAULT NULL COMMENT '产品',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=123 DEFAULT CHARSET=utf8 COMMENT='送货产品对账';

-- ----------------------------
-- Records of sale_checking_product
-- ----------------------------

-- ----------------------------
-- Table structure for `sale_delivery`
-- ----------------------------
DROP TABLE IF EXISTS `sale_delivery`;
CREATE TABLE `sale_delivery` (
  `id` int(12) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(2000) DEFAULT '' COMMENT '备注',
  `status` varchar(20) DEFAULT 'draft' COMMENT '状态',
  `approver` varchar(50) DEFAULT NULL COMMENT '审批人',
  `approval_time` datetime DEFAULT NULL COMMENT '审批时间',
  `approval_type` varchar(20) DEFAULT 'N' COMMENT '审批类型',
  `serial_number` varchar(20) NOT NULL COMMENT '单号',
  `customer_id` int(12) DEFAULT NULL COMMENT '客户',
  `salesman_id` int(12) DEFAULT NULL COMMENT '销售员',
  `payment_type` varchar(20) DEFAULT NULL COMMENT '付款方式',
  `settlement_type` varchar(20) DEFAULT NULL COMMENT '结款方式',
  `delivery_type` varchar(20) DEFAULT NULL COMMENT '交货方式',
  `contact` varchar(50) DEFAULT NULL COMMENT '联系人',
  `contact_phone` varchar(50) DEFAULT NULL COMMENT '联系人电话',
  `tax_rate` double(12,4) DEFAULT '0.0000' COMMENT '税率',
  `amount` decimal(14,4) DEFAULT '0.0000' COMMENT '总金额',
  `deposit` decimal(14,4) DEFAULT '0.0000' COMMENT '订金',
  `delivery_date` date DEFAULT NULL COMMENT '交货日期',
  `address` varchar(500) DEFAULT NULL COMMENT '送货地址',
  `logistics_company` varchar(50) DEFAULT NULL COMMENT '物流公司',
  `logistics_sn` varchar(50) DEFAULT NULL COMMENT '物流单号',
  `handwork_no` varchar(50) DEFAULT NULL COMMENT '手工单号',
  `dept_code` varchar(500) DEFAULT NULL COMMENT '部门代码',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=116 DEFAULT CHARSET=utf8 COMMENT='送货单';

-- ----------------------------
-- Records of sale_delivery
-- ----------------------------

-- ----------------------------
-- Table structure for `sale_delivery_product`
-- ----------------------------
DROP TABLE IF EXISTS `sale_delivery_product`;
CREATE TABLE `sale_delivery_product` (
  `id` int(12) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `remark` varchar(2000) DEFAULT '' COMMENT '备注',
  `sale_delivery_id` int(12) DEFAULT NULL COMMENT '送货单',
  `sale_order_id` int(12) DEFAULT NULL COMMENT '销售订单',
  `sale_order_product_id` int(12) DEFAULT NULL COMMENT '销售产品',
  `sale_order_delivery_id` int(12) DEFAULT NULL COMMENT '送货排程',
  `customer_id` int(12) DEFAULT NULL COMMENT '客户',
  `product_id` int(12) DEFAULT NULL COMMENT '产品',
  `price` decimal(14,4) DEFAULT '0.0000' COMMENT '单价',
  `qty` int(8) DEFAULT '0' COMMENT '数量',
  `free_qty` int(8) DEFAULT '0' COMMENT '免费数量',
  `return_qty` int(8) DEFAULT '0' COMMENT '退货数量',
  `tax_rate` double(12,4) DEFAULT '0.0000' COMMENT '税率',
  `amount` decimal(14,4) DEFAULT '0.0000' COMMENT '总金额',
  `warehouse_id` int(12) DEFAULT NULL COMMENT '仓库',
  `delivery_date` date DEFAULT NULL COMMENT '交货日期',
  `address` varchar(500) DEFAULT NULL COMMENT '送货地址',
  `customer_no` varchar(200) DEFAULT NULL COMMENT '客户单号',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=461 DEFAULT CHARSET=utf8 COMMENT='送货产品';

-- ----------------------------
-- Records of sale_delivery_product
-- ----------------------------

-- ----------------------------
-- Table structure for `sale_order`
-- ----------------------------
DROP TABLE IF EXISTS `sale_order`;
CREATE TABLE `sale_order` (
  `id` int(12) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(2000) DEFAULT '' COMMENT '备注',
  `status` varchar(20) DEFAULT 'draft' COMMENT '状态',
  `approver` varchar(50) DEFAULT NULL COMMENT '审批人',
  `approval_time` datetime DEFAULT NULL COMMENT '审批时间',
  `approval_type` varchar(20) DEFAULT 'N' COMMENT '审批类型',
  `serial_number` varchar(20) NOT NULL COMMENT '单号',
  `customer_id` int(12) DEFAULT NULL COMMENT '客户',
  `salesman_id` int(12) DEFAULT NULL COMMENT '销售员',
  `payment_type` varchar(20) DEFAULT NULL COMMENT '付款方式',
  `settlement_type` varchar(20) DEFAULT NULL COMMENT '结款方式',
  `delivery_type` varchar(20) DEFAULT NULL COMMENT '交货方式',
  `contact` varchar(50) DEFAULT NULL COMMENT '联系人',
  `contact_phone` varchar(50) DEFAULT NULL COMMENT '联系人电话',
  `tax_rate` double(12,4) DEFAULT '0.0000' COMMENT '税率',
  `amount` decimal(14,4) DEFAULT '0.0000' COMMENT '总金额',
  `deposit` decimal(14,4) DEFAULT '0.0000' COMMENT '订金',
  `delivery_date` date DEFAULT NULL COMMENT '交货日期',
  `address` varchar(500) DEFAULT NULL COMMENT '送货地址',
  `dept_code` varchar(500) DEFAULT NULL COMMENT '部门代码',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4951 DEFAULT CHARSET=utf8 COMMENT='销售订单';

-- ----------------------------
-- Records of sale_order
-- ----------------------------

-- ----------------------------
-- Table structure for `sale_order_delivery`
-- ----------------------------
DROP TABLE IF EXISTS `sale_order_delivery`;
CREATE TABLE `sale_order_delivery` (
  `id` int(12) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `remark` varchar(2000) DEFAULT '' COMMENT '备注',
  `sale_order_id` int(12) DEFAULT NULL COMMENT '销售订单',
  `sale_order_product_id` int(12) DEFAULT NULL COMMENT '销售产品',
  `customer_id` int(12) DEFAULT NULL COMMENT '客户',
  `product_id` int(12) DEFAULT NULL COMMENT '产品',
  `qty` int(8) DEFAULT '0' COMMENT '数量',
  `delivery_qty` int(8) DEFAULT '0' COMMENT '已送数量',
  `delivery_date` date DEFAULT NULL COMMENT '送货日期',
  `address` varchar(500) DEFAULT NULL COMMENT '送货地址',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4222 DEFAULT CHARSET=utf8 COMMENT='送货排程';

-- ----------------------------
-- Records of sale_order_delivery
-- ----------------------------

-- ----------------------------
-- Table structure for `sale_order_materials`
-- ----------------------------
DROP TABLE IF EXISTS `sale_order_materials`;
CREATE TABLE `sale_order_materials` (
  `id` int(12) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `remark` varchar(2000) DEFAULT '' COMMENT '备注',
  `sale_order_id` int(12) DEFAULT NULL COMMENT '销售订单',
  `customer_id` int(12) DEFAULT NULL COMMENT '客户',
  `materials_id` int(12) DEFAULT NULL COMMENT '材料',
  `qty` int(8) DEFAULT '0' COMMENT '数量',
  `return_qty` int(8) DEFAULT '0' COMMENT '退料数量',
  `supplier_id` int(12) DEFAULT NULL COMMENT '供应商',
  `delivery_date` date DEFAULT NULL COMMENT '供货日期',
  `warehouse_id` int(12) DEFAULT NULL COMMENT '仓库',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4424 DEFAULT CHARSET=utf8 COMMENT='客户带料';

-- ----------------------------
-- Records of sale_order_materials
-- ----------------------------

-- ----------------------------
-- Table structure for `sale_order_product`
-- ----------------------------
DROP TABLE IF EXISTS `sale_order_product`;
CREATE TABLE `sale_order_product` (
  `id` int(12) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `remark` varchar(2000) DEFAULT '' COMMENT '备注',
  `sale_order_id` int(12) DEFAULT NULL COMMENT '销售订单',
  `customer_id` int(12) DEFAULT NULL COMMENT '客户',
  `product_id` int(12) DEFAULT NULL COMMENT '产品',
  `price` decimal(14,4) DEFAULT '0.0000' COMMENT '单价',
  `qty` int(8) DEFAULT '0' COMMENT '数量',
  `tax_rate` double(12,4) DEFAULT '0.0000' COMMENT '税率',
  `amount` decimal(14,4) DEFAULT '0.0000' COMMENT '总金额',
  `delivery_date` date DEFAULT NULL COMMENT '交货日期',
  `address` varchar(500) DEFAULT NULL COMMENT '送货地址',
  `requirements` varchar(500) DEFAULT NULL COMMENT '产品要求',
  `sale_quotation_product_id` int(12) DEFAULT NULL COMMENT '报价产品',
  `customer_no` varchar(200) DEFAULT NULL COMMENT '�ͻ�����',
  `is_out_flow` char(1) DEFAULT 'N' COMMENT '������',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5584 DEFAULT CHARSET=utf8 COMMENT='销售产品';

-- ----------------------------
-- Records of sale_order_product
-- ----------------------------

-- ----------------------------
-- Table structure for `sale_quotation`
-- ----------------------------
DROP TABLE IF EXISTS `sale_quotation`;
CREATE TABLE `sale_quotation` (
  `id` int(12) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(2000) DEFAULT '' COMMENT '备注',
  `status` varchar(20) DEFAULT 'draft' COMMENT '状态',
  `approver` varchar(50) DEFAULT NULL COMMENT '审批人',
  `approval_time` datetime DEFAULT NULL COMMENT '审批时间',
  `approval_type` varchar(20) DEFAULT 'N' COMMENT '审批类型',
  `serial_number` varchar(20) NOT NULL COMMENT '单号',
  `customer_id` int(12) DEFAULT NULL COMMENT '客户',
  `customer_name` varchar(50) DEFAULT NULL COMMENT '客户名称',
  `quotation_date` date DEFAULT NULL COMMENT '报价日期',
  `contact` varchar(50) DEFAULT NULL COMMENT '联系人',
  `cell_phone` varchar(50) DEFAULT NULL COMMENT '联系人手机',
  `tax_rate` double(12,4) DEFAULT '0.0000' COMMENT '税率',
  `amount` decimal(14,4) DEFAULT '0.0000' COMMENT '总金额',
  `profit_rate` double(12,4) DEFAULT '0.0000' COMMENT '利润率',
  `quoter_id` int(12) DEFAULT NULL COMMENT '报价员',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COMMENT='报价单';

-- ----------------------------
-- Records of sale_quotation
-- ----------------------------

-- ----------------------------
-- Table structure for `sale_quotation_materials`
-- ----------------------------
DROP TABLE IF EXISTS `sale_quotation_materials`;
CREATE TABLE `sale_quotation_materials` (
  `id` int(12) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `sale_quotation_id` int(12) DEFAULT NULL COMMENT '报价单',
  `customer_id` int(12) DEFAULT NULL COMMENT '客户',
  `sale_quotation_product_id` int(12) DEFAULT NULL COMMENT '报价产品',
  `quotation_template_id` int(12) DEFAULT NULL COMMENT '报价工艺卡',
  `quotation_template_materials_id` int(12) DEFAULT NULL COMMENT '工艺卡材料',
  `product_id` int(12) DEFAULT NULL COMMENT '产品',
  `process_id` int(12) DEFAULT NULL COMMENT '关联工序',
  `materials_id` int(12) DEFAULT NULL COMMENT '材料',
  `formula_id` int(12) DEFAULT NULL COMMENT '计算公式',
  `times` double(12,4) DEFAULT '1.0000' COMMENT '倍率',
  `price` decimal(14,4) DEFAULT '0.0000' COMMENT '单价',
  `equipment_id` int(12) DEFAULT NULL COMMENT '设备',
  `loss_rate` double(12,4) DEFAULT '0.0000' COMMENT '损耗率(%)',
  `loss_qty` int(8) DEFAULT '0' COMMENT '固定损耗数',
  `single_size_long` int(8) DEFAULT '0' COMMENT '单个尺寸长',
  `single_size_width` int(8) DEFAULT '0' COMMENT '单个尺寸宽',
  `cutting_size_long` int(8) DEFAULT '0' COMMENT '开料尺寸长',
  `cutting_size_width` int(8) DEFAULT '0' COMMENT '开料尺寸宽',
  `is_get_process_qty` varchar(1) DEFAULT 'N' COMMENT '取工序数量',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='报价材料';

-- ----------------------------
-- Records of sale_quotation_materials
-- ----------------------------

-- ----------------------------
-- Table structure for `sale_quotation_mult`
-- ----------------------------
DROP TABLE IF EXISTS `sale_quotation_mult`;
CREATE TABLE `sale_quotation_mult` (
  `id` int(12) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `sale_quotation_id` int(12) DEFAULT NULL COMMENT '报价单',
  `qty` int(8) DEFAULT NULL COMMENT '数量',
  `tax_rate` double(12,4) DEFAULT '0.0000' COMMENT '税率',
  `amount` decimal(14,4) DEFAULT '0.0000' COMMENT '总金额',
  `price` decimal(14,4) DEFAULT '0.0000' COMMENT '单价',
  `calculate_log` mediumtext COMMENT '日志',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='多数量报价';

-- ----------------------------
-- Records of sale_quotation_mult
-- ----------------------------

-- ----------------------------
-- Table structure for `sale_quotation_mult_detail`
-- ----------------------------
DROP TABLE IF EXISTS `sale_quotation_mult_detail`;
CREATE TABLE `sale_quotation_mult_detail` (
  `id` int(12) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `sale_quotation_id` int(12) DEFAULT NULL COMMENT '报价单',
  `sale_quotation_mult_id` int(12) DEFAULT NULL COMMENT '多数量',
  `sale_quotation_product_id` int(12) DEFAULT NULL COMMENT '报价产品',
  `sale_quotation_process_id` int(12) DEFAULT NULL COMMENT '报价工序',
  `sale_quotation_materials_id` int(12) DEFAULT NULL COMMENT '报价材料',
  `quotation_template_id` int(12) DEFAULT NULL COMMENT '报价工艺卡',
  `product_id` int(12) DEFAULT NULL COMMENT '产品',
  `process_id` int(12) DEFAULT NULL COMMENT '工序',
  `materials_id` int(12) DEFAULT NULL COMMENT '材料',
  `in_qty` int(8) DEFAULT '0' COMMENT '投入数',
  `out_qty` int(8) DEFAULT '0' COMMENT '产出数',
  `process_order` int(4) DEFAULT '1' COMMENT '顺序',
  `calculate_log` text COMMENT '日志',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='多数量报价工序明细';

-- ----------------------------
-- Records of sale_quotation_mult_detail
-- ----------------------------

-- ----------------------------
-- Table structure for `sale_quotation_process`
-- ----------------------------
DROP TABLE IF EXISTS `sale_quotation_process`;
CREATE TABLE `sale_quotation_process` (
  `id` int(12) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `sale_quotation_id` int(12) DEFAULT NULL COMMENT '报价单',
  `customer_id` int(12) DEFAULT NULL COMMENT '客户',
  `sale_quotation_product_id` int(12) DEFAULT NULL COMMENT '报价产品',
  `quotation_template_id` int(12) DEFAULT NULL COMMENT '报价工艺卡',
  `quotation_template_process_id` int(12) DEFAULT NULL COMMENT '工艺卡工序',
  `product_id` int(12) DEFAULT NULL COMMENT '产品',
  `process_id` int(12) DEFAULT NULL COMMENT '工序',
  `formula_id` int(12) DEFAULT NULL COMMENT '计算公式',
  `times` double(12,4) DEFAULT '1.0000' COMMENT '倍率',
  `price` decimal(10,2) DEFAULT '0.00' COMMENT '单价',
  `equipment_id` int(12) DEFAULT NULL COMMENT '设备',
  `loss_rate` double(12,4) DEFAULT '0.0000' COMMENT '损耗率(%)',
  `loss_qty` int(8) DEFAULT '0' COMMENT '固定损耗数',
  `process_order` int(4) DEFAULT '1' COMMENT '顺序',
  `is_time_count` varchar(1) DEFAULT NULL COMMENT '是否倍率计算',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='报价工序';

-- ----------------------------
-- Records of sale_quotation_process
-- ----------------------------

-- ----------------------------
-- Table structure for `sale_quotation_product`
-- ----------------------------
DROP TABLE IF EXISTS `sale_quotation_product`;
CREATE TABLE `sale_quotation_product` (
  `id` int(12) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `sale_quotation_id` int(12) DEFAULT NULL COMMENT '报价单',
  `customer_id` int(12) DEFAULT NULL COMMENT '客户',
  `product_id` int(12) DEFAULT NULL COMMENT '产品',
  `quotation_template_id` int(12) DEFAULT NULL COMMENT '报价工艺卡',
  `production_template_id` int(12) DEFAULT NULL COMMENT '生产工艺卡',
  `sale_order_product_id` int(12) DEFAULT NULL COMMENT '销售产品',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='报价产品';

-- ----------------------------
-- Records of sale_quotation_product
-- ----------------------------

-- ----------------------------
-- Table structure for `sale_return`
-- ----------------------------
DROP TABLE IF EXISTS `sale_return`;
CREATE TABLE `sale_return` (
  `id` int(12) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(2000) DEFAULT '' COMMENT '备注',
  `status` varchar(20) DEFAULT 'draft' COMMENT '状态',
  `approver` varchar(50) DEFAULT NULL COMMENT '审批人',
  `approval_time` datetime DEFAULT NULL COMMENT '审批时间',
  `approval_type` varchar(20) DEFAULT 'N' COMMENT '审批类型',
  `serial_number` varchar(20) NOT NULL COMMENT '单号',
  `customer_id` int(12) DEFAULT NULL COMMENT '客户',
  `salesman_id` int(12) DEFAULT NULL COMMENT '销售员',
  `payment_type` varchar(20) DEFAULT NULL COMMENT '付款方式',
  `settlement_type` varchar(20) DEFAULT NULL COMMENT '结款方式',
  `delivery_type` varchar(20) DEFAULT NULL COMMENT '交货方式',
  `contact` varchar(50) DEFAULT NULL COMMENT '联系人',
  `contact_phone` varchar(50) DEFAULT NULL COMMENT '联系人电话',
  `tax_rate` double(12,4) DEFAULT '0.0000' COMMENT '税率',
  `amount` decimal(10,2) DEFAULT '0.00' COMMENT '总金额',
  `return_date` date DEFAULT NULL COMMENT '退货日期',
  `address` varchar(500) DEFAULT NULL COMMENT '退货地址',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='退货单';

-- ----------------------------
-- Records of sale_return
-- ----------------------------

-- ----------------------------
-- Table structure for `sale_return_product`
-- ----------------------------
DROP TABLE IF EXISTS `sale_return_product`;
CREATE TABLE `sale_return_product` (
  `id` int(12) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `remark` varchar(2000) DEFAULT '' COMMENT '备注',
  `sale_return_id` int(11) DEFAULT NULL COMMENT '退货单',
  `sale_delivery_id` int(11) DEFAULT NULL COMMENT '送货单',
  `sale_delivery_product_id` int(11) DEFAULT NULL COMMENT '送货产品',
  `sale_order_id` int(11) DEFAULT NULL COMMENT '销售订单',
  `sale_order_product_id` int(11) DEFAULT NULL COMMENT '销售产品',
  `sale_order_delivery_id` int(11) DEFAULT NULL COMMENT '送货排程',
  `customer_id` int(12) DEFAULT NULL COMMENT '客户',
  `product_id` int(12) DEFAULT NULL COMMENT '产品',
  `return_type` varchar(255) DEFAULT NULL COMMENT '退货类型',
  `return_rate` varchar(255) DEFAULT '100' COMMENT '退货折扣(%)',
  `qty` int(8) DEFAULT '0' COMMENT '数量',
  `price` decimal(10,2) DEFAULT '0.00' COMMENT '单价',
  `tax_rate` double(12,4) DEFAULT '0.0000' COMMENT '税率',
  `amount` decimal(10,2) DEFAULT '0.00' COMMENT '总金额',
  `warehouse_id` int(12) DEFAULT NULL COMMENT '仓库',
  `return_date` date DEFAULT NULL COMMENT '退货日期',
  `address` varchar(500) DEFAULT NULL COMMENT '送货地址',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='退货产品';

-- ----------------------------
-- Records of sale_return_product
-- ----------------------------

-- ----------------------------
-- Table structure for `sys_config`
-- ----------------------------
DROP TABLE IF EXISTS `sys_config`;
CREATE TABLE `sys_config` (
  `config_id` int(5) NOT NULL AUTO_INCREMENT COMMENT '参数主键',
  `config_name` varchar(100) DEFAULT '' COMMENT '参数名称',
  `config_key` varchar(100) DEFAULT '' COMMENT '参数键名',
  `config_value` varchar(500) DEFAULT '' COMMENT '参数键值',
  `config_type` char(1) DEFAULT 'N' COMMENT '系统内置（Y是 N否）',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`config_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COMMENT='参数配置表';

-- ----------------------------
-- Records of sys_config
-- ----------------------------

-- ----------------------------
-- Table structure for `sys_dept`
-- ----------------------------
DROP TABLE IF EXISTS `sys_dept`;
CREATE TABLE `sys_dept` (
  `dept_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '部门id',
  `parent_id` bigint(20) DEFAULT '0' COMMENT '父部门id',
  `ancestors` varchar(50) DEFAULT '' COMMENT '祖级列表',
  `dept_name` varchar(30) DEFAULT '' COMMENT '部门名称',
  `order_num` int(4) DEFAULT '0' COMMENT '显示顺序',
  `leader` varchar(20) DEFAULT NULL COMMENT '负责人',
  `phone` varchar(11) DEFAULT NULL COMMENT '联系电话',
  `email` varchar(50) DEFAULT NULL COMMENT '邮箱',
  `status` char(1) DEFAULT '0' COMMENT '部门状态（0正常 1停用）',
  `del_flag` char(1) DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`dept_id`)
) ENGINE=InnoDB AUTO_INCREMENT=205 DEFAULT CHARSET=utf8 COMMENT='部门表';

-- ----------------------------
-- Records of sys_dept
-- ----------------------------

-- ----------------------------
-- Table structure for `sys_dict_data`
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict_data`;
CREATE TABLE `sys_dict_data` (
  `dict_code` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '字典编码',
  `dict_sort` int(4) DEFAULT '0' COMMENT '字典排序',
  `dict_label` varchar(100) DEFAULT '' COMMENT '字典标签',
  `dict_value` varchar(100) DEFAULT '' COMMENT '字典键值',
  `dict_type` varchar(100) DEFAULT '' COMMENT '字典类型',
  `css_class` varchar(100) DEFAULT NULL COMMENT '样式属性（其他样式扩展）',
  `list_class` varchar(100) DEFAULT NULL COMMENT '表格回显样式',
  `is_default` char(1) DEFAULT 'N' COMMENT '是否默认（Y是 N否）',
  `status` char(1) DEFAULT '0' COMMENT '状态（0正常 1停用）',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`dict_code`)
) ENGINE=InnoDB AUTO_INCREMENT=356 DEFAULT CHARSET=utf8 COMMENT='字典数据表';

-- ----------------------------
-- Records of sys_dict_data
-- ----------------------------

-- ----------------------------
-- Table structure for `sys_dict_type`
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict_type`;
CREATE TABLE `sys_dict_type` (
  `dict_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '字典主键',
  `dict_name` varchar(100) DEFAULT '' COMMENT '字典名称',
  `dict_type` varchar(100) DEFAULT '' COMMENT '字典类型',
  `status` char(1) DEFAULT '0' COMMENT '状态（0正常 1停用）',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`dict_id`),
  UNIQUE KEY `dict_type` (`dict_type`)
) ENGINE=InnoDB AUTO_INCREMENT=153 DEFAULT CHARSET=utf8 COMMENT='字典类型表';

-- ----------------------------
-- Records of sys_dict_type
-- ----------------------------

-- ----------------------------
-- Table structure for `sys_job`
-- ----------------------------
DROP TABLE IF EXISTS `sys_job`;
CREATE TABLE `sys_job` (
  `job_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '任务ID',
  `job_name` varchar(64) NOT NULL DEFAULT '' COMMENT '任务名称',
  `job_group` varchar(64) NOT NULL DEFAULT 'DEFAULT' COMMENT '任务组名',
  `invoke_target` varchar(500) NOT NULL COMMENT '调用目标字符串',
  `cron_expression` varchar(255) DEFAULT '' COMMENT 'cron执行表达式',
  `misfire_policy` varchar(20) DEFAULT '3' COMMENT '计划执行错误策略（1立即执行 2执行一次 3放弃执行）',
  `concurrent` char(1) DEFAULT '1' COMMENT '是否并发执行（0允许 1禁止）',
  `status` char(1) DEFAULT '0' COMMENT '状态（0正常 1暂停）',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) DEFAULT '' COMMENT '备注信息',
  PRIMARY KEY (`job_id`,`job_name`,`job_group`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='定时任务调度表';

-- ----------------------------
-- Records of sys_job
-- ----------------------------

-- ----------------------------
-- Table structure for `sys_job_log`
-- ----------------------------
DROP TABLE IF EXISTS `sys_job_log`;
CREATE TABLE `sys_job_log` (
  `job_log_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '任务日志ID',
  `job_name` varchar(64) NOT NULL COMMENT '任务名称',
  `job_group` varchar(64) NOT NULL COMMENT '任务组名',
  `invoke_target` varchar(500) NOT NULL COMMENT '调用目标字符串',
  `job_message` varchar(500) DEFAULT NULL COMMENT '日志信息',
  `status` char(1) DEFAULT '0' COMMENT '执行状态（0正常 1失败）',
  `exception_info` varchar(2000) DEFAULT '' COMMENT '异常信息',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`job_log_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='定时任务调度日志表';

-- ----------------------------
-- Records of sys_job_log
-- ----------------------------

-- ----------------------------
-- Table structure for `sys_logininfor`
-- ----------------------------
DROP TABLE IF EXISTS `sys_logininfor`;
CREATE TABLE `sys_logininfor` (
  `info_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '访问ID',
  `login_name` varchar(50) DEFAULT '' COMMENT '登录账号',
  `ipaddr` varchar(128) DEFAULT '' COMMENT '登录IP地址',
  `login_location` varchar(255) DEFAULT '' COMMENT '登录地点',
  `browser` varchar(50) DEFAULT '' COMMENT '浏览器类型',
  `os` varchar(50) DEFAULT '' COMMENT '操作系统',
  `status` char(1) DEFAULT '0' COMMENT '登录状态（0成功 1失败）',
  `msg` varchar(255) DEFAULT '' COMMENT '提示消息',
  `login_time` datetime DEFAULT NULL COMMENT '访问时间',
  PRIMARY KEY (`info_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2407 DEFAULT CHARSET=utf8 COMMENT='系统访问记录';

-- ----------------------------
-- Records of sys_logininfor
-- ----------------------------

-- ----------------------------
-- Table structure for `sys_menu`
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu` (
  `menu_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '菜单ID',
  `menu_name` varchar(50) NOT NULL COMMENT '菜单名称',
  `parent_id` bigint(20) DEFAULT '0' COMMENT '父菜单ID',
  `order_num` int(4) DEFAULT '0' COMMENT '显示顺序',
  `url` varchar(200) DEFAULT '#' COMMENT '请求地址',
  `target` varchar(20) DEFAULT '' COMMENT '打开方式（menuItem页签 menuBlank新窗口）',
  `menu_type` char(1) DEFAULT '' COMMENT '菜单类型（M目录 C菜单 F按钮）',
  `visible` char(1) DEFAULT '0' COMMENT '菜单状态（0显示 1隐藏）',
  `is_refresh` char(1) DEFAULT '1' COMMENT '是否刷新（0刷新 1不刷新）',
  `perms` varchar(100) DEFAULT NULL COMMENT '权限标识',
  `icon` varchar(100) DEFAULT '#' COMMENT '菜单图标',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) DEFAULT '' COMMENT '备注',
  PRIMARY KEY (`menu_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2483 DEFAULT CHARSET=utf8 COMMENT='菜单权限表';

-- ----------------------------
-- Records of sys_menu
-- ----------------------------

-- ----------------------------
-- Table structure for `sys_notice`
-- ----------------------------
DROP TABLE IF EXISTS `sys_notice`;
CREATE TABLE `sys_notice` (
  `notice_id` int(4) NOT NULL AUTO_INCREMENT COMMENT '公告ID',
  `notice_title` varchar(50) NOT NULL COMMENT '公告标题',
  `notice_type` char(1) NOT NULL COMMENT '公告类型（1通知 2公告）',
  `notice_content` varchar(2000) DEFAULT NULL COMMENT '公告内容',
  `status` char(1) DEFAULT '0' COMMENT '公告状态（0正常 1关闭）',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`notice_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='通知公告表';

-- ----------------------------
-- Records of sys_notice
-- ----------------------------

-- ----------------------------
-- Table structure for `sys_oper_log`
-- ----------------------------
DROP TABLE IF EXISTS `sys_oper_log`;
CREATE TABLE `sys_oper_log` (
  `oper_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '日志主键',
  `title` varchar(50) DEFAULT '' COMMENT '模块标题',
  `business_type` int(2) DEFAULT '0' COMMENT '业务类型（0其它 1新增 2修改 3删除）',
  `method` varchar(500) DEFAULT '' COMMENT '方法名称',
  `request_method` varchar(10) DEFAULT '' COMMENT '请求方式',
  `operator_type` int(1) DEFAULT '0' COMMENT '操作类别（0其它 1后台用户 2手机端用户）',
  `oper_name` varchar(50) DEFAULT '' COMMENT '操作人员',
  `dept_name` varchar(50) DEFAULT '' COMMENT '部门名称',
  `oper_url` varchar(255) DEFAULT '' COMMENT '请求URL',
  `oper_ip` varchar(128) DEFAULT '' COMMENT '主机地址',
  `oper_location` varchar(255) DEFAULT '' COMMENT '操作地点',
  `oper_param` varchar(2000) DEFAULT '' COMMENT '请求参数',
  `json_result` varchar(2000) DEFAULT '' COMMENT '返回参数',
  `status` int(1) DEFAULT '0' COMMENT '操作状态（0正常 1异常）',
  `error_msg` varchar(2000) DEFAULT '' COMMENT '错误消息',
  `oper_time` datetime DEFAULT NULL COMMENT '操作时间',
  PRIMARY KEY (`oper_id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COMMENT='操作日志记录';

-- ----------------------------
-- Records of sys_oper_log
-- ----------------------------

-- ----------------------------
-- Table structure for `sys_post`
-- ----------------------------
DROP TABLE IF EXISTS `sys_post`;
CREATE TABLE `sys_post` (
  `post_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '岗位ID',
  `post_code` varchar(64) NOT NULL COMMENT '岗位编码',
  `post_name` varchar(50) NOT NULL COMMENT '岗位名称',
  `post_sort` int(4) NOT NULL COMMENT '显示顺序',
  `status` char(1) NOT NULL COMMENT '状态（0正常 1停用）',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`post_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COMMENT='岗位信息表';

-- ----------------------------
-- Records of sys_post
-- ----------------------------

-- ----------------------------
-- Table structure for `sys_role`
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role` (
  `role_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '角色ID',
  `role_name` varchar(30) NOT NULL COMMENT '角色名称',
  `role_key` varchar(100) NOT NULL COMMENT '角色权限字符串',
  `role_sort` int(4) NOT NULL COMMENT '显示顺序',
  `data_scope` char(1) DEFAULT '1' COMMENT '数据范围（1：全部数据权限 2：自定数据权限 3：本部门数据权限 4：本部门及以下数据权限）',
  `status` char(1) NOT NULL COMMENT '角色状态（0正常 1停用）',
  `del_flag` char(1) DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=105 DEFAULT CHARSET=utf8 COMMENT='角色信息表';

-- ----------------------------
-- Records of sys_role
-- ----------------------------

-- ----------------------------
-- Table structure for `sys_role_dept`
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_dept`;
CREATE TABLE `sys_role_dept` (
  `role_id` bigint(20) NOT NULL COMMENT '角色ID',
  `dept_id` bigint(20) NOT NULL COMMENT '部门ID',
  PRIMARY KEY (`role_id`,`dept_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='角色和部门关联表';

-- ----------------------------
-- Records of sys_role_dept
-- ----------------------------

-- ----------------------------
-- Table structure for `sys_role_menu`
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_menu`;
CREATE TABLE `sys_role_menu` (
  `role_id` bigint(20) NOT NULL COMMENT '角色ID',
  `menu_id` bigint(20) NOT NULL COMMENT '菜单ID',
  PRIMARY KEY (`role_id`,`menu_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='角色和菜单关联表';

-- ----------------------------
-- Records of sys_role_menu
-- ----------------------------

-- ----------------------------
-- Table structure for `sys_user`
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user` (
  `user_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '用户ID',
  `dept_id` bigint(20) DEFAULT NULL COMMENT '部门ID',
  `login_name` varchar(30) NOT NULL COMMENT '登录账号',
  `user_name` varchar(30) DEFAULT '' COMMENT '用户昵称',
  `user_type` varchar(2) DEFAULT '00' COMMENT '用户类型（00系统用户 01注册用户）',
  `email` varchar(50) DEFAULT '' COMMENT '用户邮箱',
  `phonenumber` varchar(11) DEFAULT '' COMMENT '手机号码',
  `sex` char(1) DEFAULT '0' COMMENT '用户性别（0男 1女 2未知）',
  `avatar` varchar(100) DEFAULT '' COMMENT '头像路径',
  `password` varchar(50) DEFAULT '' COMMENT '密码',
  `salt` varchar(20) DEFAULT '' COMMENT '盐加密',
  `status` char(1) DEFAULT '0' COMMENT '帐号状态（0正常 1停用）',
  `del_flag` char(1) DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
  `login_ip` varchar(128) DEFAULT '' COMMENT '最后登录IP',
  `login_date` datetime DEFAULT NULL COMMENT '最后登录时间',
  `pwd_update_date` datetime DEFAULT NULL COMMENT '密码最后更新时间',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COMMENT='用户信息表';

-- ----------------------------
-- Records of sys_user
-- ----------------------------

-- ----------------------------
-- Table structure for `sys_user_online`
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_online`;
CREATE TABLE `sys_user_online` (
  `sessionId` varchar(50) NOT NULL DEFAULT '' COMMENT '用户会话id',
  `login_name` varchar(50) DEFAULT '' COMMENT '登录账号',
  `dept_name` varchar(50) DEFAULT '' COMMENT '部门名称',
  `ipaddr` varchar(128) DEFAULT '' COMMENT '登录IP地址',
  `login_location` varchar(255) DEFAULT '' COMMENT '登录地点',
  `browser` varchar(50) DEFAULT '' COMMENT '浏览器类型',
  `os` varchar(50) DEFAULT '' COMMENT '操作系统',
  `status` varchar(10) DEFAULT '' COMMENT '在线状态on_line在线off_line离线',
  `start_timestamp` datetime DEFAULT NULL COMMENT 'session创建时间',
  `last_access_time` datetime DEFAULT NULL COMMENT 'session最后访问时间',
  `expire_time` int(5) DEFAULT '0' COMMENT '超时时间，单位为分钟',
  PRIMARY KEY (`sessionId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='在线用户记录';

-- ----------------------------
-- Records of sys_user_online
-- ----------------------------

-- ----------------------------
-- Table structure for `sys_user_post`
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_post`;
CREATE TABLE `sys_user_post` (
  `user_id` bigint(20) NOT NULL COMMENT '用户ID',
  `post_id` bigint(20) NOT NULL COMMENT '岗位ID',
  PRIMARY KEY (`user_id`,`post_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户与岗位关联表';

-- ----------------------------
-- Records of sys_user_post
-- ----------------------------

-- ----------------------------
-- Table structure for `sys_user_role`
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role` (
  `user_id` bigint(20) NOT NULL COMMENT '用户ID',
  `role_id` bigint(20) NOT NULL COMMENT '角色ID',
  PRIMARY KEY (`user_id`,`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户和角色关联表';

-- ----------------------------
-- Records of sys_user_role
-- ----------------------------

-- ----------------------------
-- Table structure for `warehouse_inventory`
-- ----------------------------
DROP TABLE IF EXISTS `warehouse_inventory`;
CREATE TABLE `warehouse_inventory` (
  `id` int(12) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(2000) DEFAULT '' COMMENT '备注',
  `status` varchar(20) DEFAULT 'vaild' COMMENT '状态',
  `warehouse_id` int(12) NOT NULL COMMENT '仓库',
  `warehouse_type` varchar(20) DEFAULT 'common' COMMENT '物品类型',
  `product_id` int(12) DEFAULT NULL COMMENT '产品',
  `materials_id` int(12) DEFAULT NULL COMMENT '材料',
  `goods_name` varchar(50) DEFAULT NULL COMMENT '物品名称',
  `qty` int(8) DEFAULT '0' COMMENT '数量',
  `unused_qty` int(8) DEFAULT '0' COMMENT '不可用数量',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16245 DEFAULT CHARSET=utf8 COMMENT='库存统计';

-- ----------------------------
-- Records of warehouse_inventory
-- ----------------------------

-- ----------------------------
-- Table structure for `warehouse_record`
-- ----------------------------
DROP TABLE IF EXISTS `warehouse_record`;
CREATE TABLE `warehouse_record` (
  `id` int(12) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(2000) DEFAULT '' COMMENT '备注',
  `status` varchar(20) DEFAULT 'vaild' COMMENT '状态',
  `warehouse_id` int(12) NOT NULL COMMENT '仓库',
  `record_source` varchar(20) DEFAULT NULL COMMENT '记录来源',
  `qty` int(8) DEFAULT '0' COMMENT '数量',
  `product_id` int(12) DEFAULT NULL COMMENT '产品',
  `materials_id` int(12) DEFAULT NULL COMMENT '材料',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8236 DEFAULT CHARSET=utf8 COMMENT='出入库记录';

-- ----------------------------
-- Records of warehouse_record
-- ----------------------------

-- ----------------------------
-- Procedure structure for `reorganize_inventory`
-- ----------------------------
DROP PROCEDURE IF EXISTS `reorganize_inventory`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `reorganize_inventory`()
BEGIN
	DECLARE V_STATUS VARCHAR(20);
	DECLARE V_warehouse_id INTEGER;
DECLARE V_product_id INTEGER;
DECLARE V_QTY INTEGER;
DECLARE V_materials_id INTEGER;


SELECT
	wi. STATUS,
	wi.warehouse_id,
	wi.product_id,
	wi.materials_id,
	sum(wi.qty)
INTO
V_STATUS,V_warehouse_id,V_product_id,V_materials_id,V_QTY
FROM
	warehouse_inventory wi
GROUP BY
	wi. STATUS,
	wi.warehouse_id,
	wi.product_id,
	wi.materials_id
HAVING
	count(wi.id) > 1;

END
;;
DELIMITER ;
